import os
import cv2

path = r"E:\Trainingsdaten_Master\Trainingsdaten\Trainingsdatensaetze\Datensatz_Klassifikation\train\cad\Wuerfel.png"

img = cv2.imread(path)
imgL = cv2.Laplacian(img, cv2.CV_64F)
canny = cv2.Canny(img, 100, 200)
os.chdir("E:\Trainingsdaten_Master\Trainingsdaten\Trainingsdatensaetze\Datensatz_Sobel\Laplacian\CAD")
cv2.imwrite("Sobel_Laplacian_cad.png",imgL)
os.chdir("E:\Trainingsdaten_Master\Trainingsdaten\Trainingsdatensaetze\Datensatz_Sobel\Canny\CAD")
cv2.imwrite("Sobel_Canny_cad.png", canny)

