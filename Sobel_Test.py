#https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_gradients/py_gradients.html

import cv2
import numpy as np
import os, os.path
from matplotlib import pyplot as plt
from cv2 import Laplacian, Sobel

img = cv2.imread(r"E:\Trainingsdaten\Wuerfel\rohdaten\Wuerfel_20mm_29\1517.png")

laplacian = cv2.Laplacian(img,cv2.CV_64F)
canny = cv2.Canny(img,100,200)
sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=5)
sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=5)

imgboth = cv2.addWeighted(sobelx, 0.5, sobely, 0.5, 0)

plt.subplot(2,2,1),plt.imshow(img)
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,2),plt.imshow(laplacian)
plt.title('Laplacian'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,3),plt.imshow(canny)
plt.title('Canny'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,4),plt.imshow(imgboth)
plt.title('Combination'), plt.xticks([]), plt.yticks([])

plt.show()


directory = r"C:\Users\User\ml\Bilder"
content = os.listdir(directory)

for i in range(0, len(content)):
    os.chdir(directory)
    print(str(content[i]))

    # Bild einlesen
    img = cv2.imread(str(content[i]))
    plt.imshow(img, cmap = 'gray')

    # Sobel X
    imgx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=5)

    # Sobel Y
    imgy = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=5)

    # Laplacian
    imgL = cv2.Laplacian(img,cv2.CV_64F)

    # Combination of Sobel X and Y
    imgboth = cv2.addWeighted(imgx, 0.5, imgy, 0.5, 0)

    # Canny
    canny = cv2.Canny(img,100,200)
    # Bilder speichern
    os.chdir( r"C:\Users\User\ml\Sobel")
    #cv2.imwrite("Sobel_X_" + content[i], imgx)
    #cv2.imwrite("Sobel_y_" + content[i], imgy)
    #cv2.imwrite("Sobel_Combination_ " + content[i], imgboth)
    #cv2.imwrite("Sobel_laplacian_" + content[i], imgL)
    #cv2.imwrite("Sobel_Canny_" + content[i], canny)
