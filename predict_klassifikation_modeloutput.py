from __future__ import absolute_import, division, print_function, unicode_literals
from itertools import *
import tensorflow as tf
from tensorflow.keras import datasets, layers, models
import numpy as np
import matplotlib.pyplot as plt
import os
import pathlib
from sklearn import metrics
from pathlib import Path
from datetime import datetime
import re
import sys

AUTOTUNE = tf.data.experimental.AUTOTUNE

class_names = [ 'gut', 'layered-shift', 'clogged_nozlle', 'spaghetti']

data_dir = r"F:/Trainingsdaten_Master/predictions_small"

data_dir = pathlib.Path(data_dir)

# Dataset aus nur einem Bild erstellen
list_ds = tf.data.Dataset.list_files([str(data_dir / '*/*')], shuffle=False)

file_path = next(iter(list_ds))
print(file_path)


# Reads an image from a file, decodes it into a dense tensor, and resizes it
# to a fixed shape.
def show(image, label):
  plt.figure()
  plt.imshow(image)
  plt.axis('off')
  plt.title(label.numpy())
  plt.show()


import scipy.ndimage as ndimage

def random_rotate_image(image):
  image = ndimage.rotate(image, np.random.uniform(-10, 10), reshape=False)
  return image

def tf_random_rotate_image(image, label):
  im_shape = image.shape
  [image,] = tf.py_function(random_rotate_image, [image], [tf.float32])
  image.set_shape(im_shape)
  return image, label

def get_label(filename):
    parts = tf.strings.split(filename, os.path.sep)


    def gut(): return tf.constant(0)
    def layershift(): return tf.constant(1)
    def clogged_nozzle(): return tf.constant(2)
    def spaghetti(): return tf.constant(3)

    label = tf.case([(parts[-2] == tf.constant('gut'), gut),
                     (parts[-2] == tf.constant('layershift'), layershift),
                     (parts[-2] == tf.constant('clogged_nozzle'), clogged_nozzle),
                     (parts[-2] == tf.constant('spaghetti'), spaghetti)
                     ])
    return label

def parse_image(filename):

    label = get_label(filename)
    image = tf.io.read_file(filename)
    image = tf.image.decode_png(image, channels=1)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize(image, [140, 310])

    return  image, label


# Dataset bearbeiten
predict_ds = list_ds.map(parse_image, num_parallel_calls=AUTOTUNE)
label_ds = list_ds.map(get_label , num_parallel_calls=AUTOTUNE)
#print(predict_ds)

label = []

test_ds = list_ds.map(parse_image)
# Dataset in Batchsize
predict_ds = predict_ds.batch(1)



# gespeichertes Netz laden
model = tf.keras.models.load_model(
    r'F:\Training_output_Klassifikation\Modell_speicher\aktuell\spaghetti\alexnet')
model.summary()

# Vorhersage für Dataset treffen
pred = model.predict(predict_ds, verbose=1)

length = len(pred)

for i in label_ds:
    label.append(i)
label = tf.dtypes.cast(label, tf.int32)

rounded = tf.math.argmax(pred, axis=-1, output_type=tf.int32)


matrix = metrics.confusion_matrix(label, rounded)
print(matrix)

for i in range(1,length):
    print(label[i],rounded[i])

lenth = len(pred)

for i in range(1 ,lenth):
  print(i , pred[i][0])


'''
for image in predict_ds[0].take(100):
    show(image, rounded)

for count,image in enumerate(predict_ds[0].take(100)):
    show(image,rounded[count])
'''