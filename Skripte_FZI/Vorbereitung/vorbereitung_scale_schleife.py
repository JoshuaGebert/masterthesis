#https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_gradients/py_gradients.html

import cv2
import numpy as np
import os, os.path
from matplotlib import pyplot as plt
from cv2 import Laplacian, Sobel
import pathlib
from pathlib import Path


# Ordner mit Inhalt Struktur der Daten
data_dir = r"E:\Vorbereitung_testdatensatz"
data_dir = Path(data_dir)  # liest den Pfad ein

label_dir = data_dir
label_names = np.array([item.name for item in label_dir.glob('*')])


def linear_skale(image_bild, min, max):
    img1 = np.zeros(shape=(140, 310), dtype='uint8')
    image = image_bild
    minimum = min
    maximum = max

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            if image[y][x] < minimum:
                minimum = image[y][x]
            elif image[y][x] > maximum:
                maximum = image[y][x]


    # Grauwerte anpassen
    schwarz = 0
    weiß = 255

    c2 = (weiß - schwarz) / (maximum - minimum)
    c1 = (minimum) * -1

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            if (c2 * image[y][x] + c1 * c2) < 0:
                img1[y][x] = 0
            elif (c2 * image[y][x] + c1 * c2) > 255:
                img1[y][x] = 255
            else:
                img1[y][x] = (image[y][x] + c1) * c2

    return img1


def linear_skale_1(image_bild, min, max):
    img1 = np.zeros(shape=(140, 310), dtype='uint8')
    image = image_bild
    minimum = min
    maximum = max

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            if image[y][x] < minimum:
                minimum = image[y][x]
            elif image[y][x] > maximum:
                maximum = image[y][x]

    # Grauwerte anpassen
    schwarz = 0
    weiß = 255

    c2 = (weiß - schwarz) / (maximum - minimum)
    c1 = (minimum) * -1

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            img1[y][x] = (image[y][x] + c1) * c2

    return img1



for a in label_names:
    print(a)


for item in label_names:

    img = cv2.imread(r"E:/Vorbereitung_testdatensatz/"+ item)

    img = img[470:610, 955:1265]  # aktuell: [490:630, 925:1235]

    # Graustufen
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # img = cv2.cvtColor(img, cv2.COLOR_BGR2BGRA)

    # Bild drehen
    img = cv2.rotate(img, cv2.ROTATE_180)
    img_scale = linear_skale(img,20,180)
    img_scale_1 = linear_skale(img, 20, 255)
    img_scale_2 = linear_skale_1(img, 20, 180)
    img_scale_3 = linear_skale_1(img, 20, 255)

    # vorher (img,42,162)


    hist1 = cv2.calcHist([img],[0],None,[256],[0,256])
    hist2 = cv2.calcHist([img_scale],[0],None,[256],[0,256])
    hist3 = cv2.calcHist([img_scale_1], [0], None, [256], [0, 256])
    hist4 = cv2.calcHist([img_scale_2], [0], None, [256], [0, 256])
    hist5 = cv2.calcHist([img_scale_3], [0], None, [256], [0, 256])
    #hist4 = cv2.calcHist([img_rescale_3],[0],None,[256],[0,256])
    plt.plot(hist1)
    plt.title('Original')
    plt.show()

    plt.plot(hist2)
    plt.title('scale_j_20_180')
    plt.show()

    plt.plot(hist3)
    plt.title('scale_j_20_250')
    plt.show()
    plt.plot(hist4)
    plt.title('scale_ohne_20_180')
    plt.show()

    plt.plot(hist5)
    plt.title('scale_ohne_20_250')
    plt.show()

    plt.subplot(2,2,1),plt.imshow(img_scale , cmap = plt.cm.gray)
    plt.title('j_20_180'), plt.xticks([]), plt.yticks([])
    plt.subplot(2,2,2),plt.imshow(img_scale_1 , cmap = plt.cm.gray)
    plt.title('j_20_255'), plt.xticks([]), plt.yticks([])
    plt.subplot(2,2,3),plt.imshow(img_scale_2 , cmap = plt.cm.gray)
    plt.title('ohne_20_180'), plt.xticks([]), plt.yticks([])
    plt.subplot(2,2,4),plt.imshow(img_scale_3, cmap = plt.cm.gray)
    plt.title('ohne_20_255'), plt.xticks([]), plt.yticks([])
    plt.show()
    '''

    print(item)
    os.chdir(r"E:/Vorbereitung_testdatensatz/")
    #cv2.imwrite("Ueberarbeitet_gamma_2_" + item, img_gamma_1)
    #cv2.imwrite("Ueberarbeitet_gamma_1_" + item, img_gamma)
    #cv2.imwrite("Ueberarbeitet_linscale_20_180_" + item, img_scale)
    #cv2.imwrite("Ueberarbeitet_normal_" + item, img)
    #cv2.imwrite("Ueberarbeitet_gamma_2_" + item, hist4)
    #cv2.imwrite("Ueberarbeitet_gamma_0.4_" + item, hist3)
    #cv2.imwrite("Ueberarbeitet_linscale_" + item, hist2)
    '''

