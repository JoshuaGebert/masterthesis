import numpy as np

import os
import cv2
import pathlib
from pathlib import Path


# Ordner mit Inhalt Struktur der Daten
data_dir = r"F:\Trainingsdaten\Ueberarbeitet\labels"
data_dir = Path(data_dir)  # liest den Pfad ein

# Labels in Array schreiben
label_dir = data_dir
label_names = np.array([item.name for item in label_dir.glob('*')])

for xy in label_names:
    print(xy)
    p = Path(r"F:/Trainingsdaten/Ueberarbeitet/labels/" + xy)
    #os.mkdir(r"F:/Trainingsdaten_Master/Trainingsdatensaetze/Datensatz_Wuerfel_layershift_neu/" + xy)
    test_names = np.array([item.name for item in p.glob('*')])
    for z in test_names:
        print(z)
        p2 = Path(r"F:/Trainingsdaten/Ueberarbeitet/labels/" + xy + "/" + z)
        content = os.listdir(p2)
        for t in range(0, len(content)):
            print(str(content[t]))
            os.chdir(p2)
            img = cv2.imread(str(content[t]))

            os.chdir(r"F:/Trainingsdaten_Master/" + xy)
            cv2.imwrite(str(xy) + "_" + str(z) + "_" + str(t) + ".png", img)



