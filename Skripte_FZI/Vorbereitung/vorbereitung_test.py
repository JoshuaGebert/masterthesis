#https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_gradients/py_gradients.html

import cv2
import numpy as np
import math
import os, os.path
from matplotlib import pyplot as plt
from cv2 import Laplacian, Sobel

img = cv2.imread(r"F:\Trainingsdaten\Rohdaten\Wuerfel\Wuerfel_20mm_46\1041.png")

img = img[460:600, 955:1265]  # aktuell: [490:630, 925:1235]


# Graustufen
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# img = cv2.cvtColor(img, cv2.COLOR_BGR2BGRA)

# Bild drehen
img = cv2.rotate(img, cv2.ROTATE_180)

def exponential(image):
    img1 = np.zeros(shape=(140, 310), dtype='uint8')
    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
           img1[y][x] =  math.exp(image[y][x]*0.02176)-1

    return img1




def linear_skale(image_bild, min, max):
    img1 = np.zeros(shape=(140, 310), dtype='uint8')
    image = image_bild
    minimum = min
    maximum = max

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            print(image[y][x])
            if image[y][x] < minimum:
                            minimum = image[y][x]
            elif image[y][x] > maximum:
                            maximum = image[y][x]



    print(minimum, maximum)
    # Grauwerte anpassen
    schwarz = 0
    weiß = 255

    c2 = (weiß - schwarz) / (maximum - minimum)
    c1 = (minimum) * -1
    print(c1)
    print(c2)

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            if (c2*image[y][x] + c1*c2) < 0:
                img1[y][x] = 0
            elif (c2*image[y][x] + c1*c2) > 255:
                img1[y][x] = 255
            else: img1[y][x] = (image[y][x] + c1)*c2

    return img1

def linear_scale_update(image_bild, c_1, c_2):
    img1 = np.zeros(shape=(140, 310), dtype='uint8')
    image = image_bild

    c2 = c_2
    c1 = c_1
    print(c1)
    print(c2)

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            if (c2*image[y][x] + c1*c2) < 0:
                img1[y][x] = 0
            elif (c2*image[y][x] + c1*c2) > 255:
                img1[y][x] = 255
            else: img1[y][x] = (image[y][x] + c1)*c2

    return img1



def gamma_correction(image, gamma):
    lookUpTable = np.empty((1, 256), np.uint8)
    for i in range(256):
        lookUpTable[0, i] = np.clip(pow(i / 255.0, gamma) * 255.0, 0, 255)
    res = cv2.LUT(image, lookUpTable)
    return res

def gamma(image, gamma):
    img1 = np.zeros(shape=(140, 310), dtype='uint8')
    maximum = 127
    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            print(image[y][x])
            if image[y][x] > maximum:
                            maximum = image[y][x]



    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            img1[y][x] = 255*(img[y][x]/maximum)**(1/gamma)

    return img1

def clipping(image):
    img1 = np.zeros(shape=(140, 310), dtype='uint8')
    minimum = 127
    maximum = 127

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            print(image[y][x])
            if image[y][x] < minimum:
                            minimum = image[y][x]
            elif image[y][x] > maximum:
                            maximum = image[y][x]

    # Grauwerte anpassen
    schwarz = 0
    weiß = 255

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            if (image[y][x] > 0) and (image[y][x] < minimum):
                img1[y][x] = 0
            elif (image[y][x] > minimum) and (image[y][x] < maximum):
                img1[y][x] = (255/(maximum-minimum))*(image[y][x]-minimum)
            elif image[y][x] > maximum:
                img1[y][x] = 255

    return img1

i = 1


img_gamma_08 = gamma(img,0.8)
img_gamma_12 = gamma(img,1.2)
img_clipping = clipping(img)
img_lin_1 = linear_scale_update(img, 10,0.3)
img_lin_2 = linear_scale_update(img, 50,0.3)
img_lin = linear_skale(img,50,127)


hist1 = cv2.calcHist([img],[0],None,[256],[0,256])
hist2 = cv2.calcHist([img_gamma_08],[0],None,[256],[0,256])
hist3 = cv2.calcHist([img_lin_1],[0],None,[256],[0,256])
hist4 = cv2.calcHist([img_lin_2],[0],None,[256],[0,256])
#hist4 = cv2.calcHist([img_rescale_3],[0],None,[256],[0,256])

plt.plot(hist1)
plt.title('normal')
plt.show()
plt.plot(hist2)
plt.title('gamma08')
plt.show()
plt.plot(hist3)
plt.title('lin40-15')
plt.show()
plt.plot(hist4)
plt.title('lin20-125')
plt.show()

plt.subplot(2,2,1),plt.imshow(img , cmap = plt.cm.gray)
plt.title('original'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,2),plt.imshow(img_lin_1 , cmap = plt.cm.gray)
plt.title('lin-40-2'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,3),plt.imshow(img_lin , cmap = plt.cm.gray)
plt.title('jonas'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,4),plt.imshow(img_gamma_08, cmap = plt.cm.gray)
plt.title('lin-20-2'), plt.xticks([]), plt.yticks([])
plt.show()
'''

pixels = np.asarray(img)

np.savetxt("pixel_data_normal.csv", pixels, delimiter=",")
pixels_1 = np.asarray(img_gamma_08)

np.savetxt("pixel_data_gamma.csv", pixels_1, delimiter=",")


'''
