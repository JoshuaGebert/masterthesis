import numpy as np

import os
import cv2
import pathlib
from pathlib import Path

i = 1
def gamma(image, gamma):
    img1 = np.zeros(shape=(140, 310), dtype='uint8')
    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            img1[y][x] = 255*(img[y][x]/255)**(1/gamma)

    return img1

def clipping(image):
    img1 = np.zeros(shape=(140, 310), dtype='uint8')
    minimum = 127
    maximum = 127

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            if image[y][x] < minimum:
                            minimum = image[y][x]
            elif image[y][x] > maximum:
                            maximum = image[y][x]

    # Grauwerte anpassen
    schwarz = 0
    weiß = 255

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            if (image[y][x] > 0) and (image[y][x] < minimum):
                img1[y][x] = 0
            elif (image[y][x] > minimum) and (image[y][x] < maximum):
                img1[y][x] = (255/(maximum-minimum))*(image[y][x]-minimum)
            elif image[y][x] > maximum:
                img1[y][x] = 255

    return img1


# Ordner mit Inhalt Struktur der Daten
data_dir = r"F:\Trainingsdaten\Rohdaten\Sphere\rohdaten"
data_dir = Path(data_dir)  # liest den Pfad ein

# Labels in Array schreiben
label_dir = data_dir
label_names = np.array([item.name for item in label_dir.glob('*')])


for xy in label_names:
    print(xy)
    p = Path( r"F:/Trainingsdaten/Rohdaten/Sphere/rohdaten/" + xy)
    #os.mkdir(r"F:/Trainingsdaten_Master/Trainingsdatensaetze/Datensatz_Wuerfel_layershift_neu/" + xy)
    test_names = np.array([item.name for item in p.glob('*')])
    for z in test_names:
        p2 = Path(r"F:/Trainingsdaten_Master/Trainingsdatensaetze/Datensatz_Wuerfel_layershift/train/labels/" + xy + "/" + z)
        content = os.listdir(p2)
        for t in range(0, len(content)):
            print(str(content[t]))
            os.chdir(p2)
            img = cv2.imread(str(content[t]))

            os.chdir(r"F:/Trainingsdaten_Master/Trainingsdatensaetze/Datensatz_Wuerfel_layershift_neu/" + xy)
            cv2.imwrite("Bild_" + str(xy) + "_" + str(z) + "_" + str(t) + ".png", img)



