import numpy as np

import os
import cv2
import pathlib
from pathlib import Path

i = 1

def linear_scale_minmax(image):
    img1 = np.zeros(shape=(140,310), dtype='uint8')

    # minimalen/maximalen Grauwert ermitteln
    minimum = 127
    maximum = 127

    for x in range(0, img.shape[1]):
        for y in range(0, img.shape[0]):
            if img[y][x] < minimum:
                minimum = img[y][x]
            elif img[y][x] > maximum:
                maximum = img[y][x]

    c_1 = minimum * (-1)
    c_2 = 255 / (maximum - minimum)

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            if (img[y][x] + c_1) * c_2 > 255:
                img1[y][x] = 255
            elif (img[y][x] + c_1) * c_2 < 0:
                img1[y][x] = 0
            else:
                img1[y][x] = (img[y][x] + c_1) * c_2

    return img1

def linear_scale(image,c1, c2):
    img1 = np.zeros(shape=(140,310), dtype='uint8')

    c_1 = c1 * (-1)
    c_2 = c2

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            if (img[y][x] + c_1) * c_2 > 255:
                img1[y][x] = 255
            elif (img[y][x] + c_1) * c_2 < 0:
                img1[y][x] = 0
            else:
                img1[y][x] = (img[y][x] + c_1) * c_2

    return img1



def gamma(image, gamma):
    img1 = np.zeros(shape=(140, 310), dtype='uint8')
    maximum = 127
    for x in range(0, image.shape[1]):
       for y in range(0, image.shape[0]):
            #print(image[y][x])
            if image[y][x] > maximum:
                           maximum = image[y][x]

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            img1[y][x] = 255*(img[y][x]/maximum)**(1/gamma)

    return img1

def clipping(image):
    img1 = np.zeros(shape=(140, 310), dtype='uint8')
    minimum = 127
    maximum = 127

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            if image[y][x] < minimum:
                            minimum = image[y][x]
            elif image[y][x] > maximum:
                            maximum = image[y][x]

    # Grauwerte anpassen
    schwarz = 0
    weiß = 255

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            if (image[y][x] > 0) and (image[y][x] < minimum):
                img1[y][x] = 0
            elif (image[y][x] > minimum) and (image[y][x] < maximum):
                img1[y][x] = (255/(maximum-minimum))*(image[y][x]-minimum)
            elif image[y][x] > maximum:
                img1[y][x] = 255

    return img1


# Ordner mit Inhalt Struktur der Daten
data_dir = r"F:\Trainingsdaten\Rohdaten\Pyramide"
data_dir = Path(data_dir)  # liest den Pfad ein

# Labels in Array schreiben
label_dir = data_dir
label_names = np.array([item.name for item in label_dir.glob('*')])

label_names_1 = []

for x in label_names:
    print(x)



for xy in label_names:
    p = Path(r"F:/Trainingsdaten/Rohdaten/Pyramide/" + xy)
    try:
        os.makedirs(r"F:/Trainingsdaten/Ueberarbeitet/pyramide/" + xy )

        print(p)
        content = os.listdir(p)
        print(len(content))
        #content.sort()

        for i in range(0, len(content)):
            print(str(content[i]))
            os.chdir(p)

            # Bild einlesen
            img = cv2.imread(str(content[i]))

             # Bild schneiden
            img = img[460:600, 955:1265]  # aktuell: [460:600, 955:1265] , [490:630, 925:1235]

                # Graustufen
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

                # Bild drehen
            img = cv2.rotate(img, cv2.ROTATE_180)

            img_update_gamma = gamma(img,0.8)
            #img_update_clipping = clipping(img)

                # Bilder speichern

            os.chdir(r"F:/Trainingsdaten/Ueberarbeitet/pyramide/" + xy )
            cv2.imwrite("Ueberarbeitet_" + str(xy) + "_" + str(content[i]), img_update_gamma)
    except:
        print("error " + str(IOError))
