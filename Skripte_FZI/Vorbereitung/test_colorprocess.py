
import cv2
import numpy as np
import os, os.path
from matplotlib import pyplot as plt
from cv2 import Laplacian, Sobel

import sys

img = cv2.imread(r"F:\Trainingsdaten\Rohdaten\Halbkugel\Sphere_20mm_1\627.png")

img = img[460:600, 955:1265]  # aktuell: [490:630, 925:1235]

img = cv2.rotate(img, cv2.ROTATE_180)

print(img.shape)


i = 1


def color_transform(image):
    img = image
    img1 = np.zeros(shape=(140, 310),dtype='uint8')
    for x in range(0, img.shape[1]):
        for y in range(0, img.shape[0]):
            r = 0
            g = 0
            summe = int
            b = 0
            i = 1
            for z in range(0, img.shape[2]):

                if (i < 2):
                    r = int(img[y][x][z])

                elif (i == 2):
                    g = int(img[y][x][z])

                elif (i > 2):
                    b = int(img[y][x][z])


                i = i + 1

            summe = (r + g + b)

            r_update = r / summe
            g_update = g / summe
            b_update = b / summe
            erg_1 = r_update * r + g_update * g + b_update * b
            erg = int(erg_1)


            img1[y][x] = erg

    return img1

def gamma_correction(image, gamma):
    lookUpTable = np.empty((1, 256), np.uint8)
    for i in range(256):
        lookUpTable[0, i] = np.clip(pow(i / 255.0, gamma) * 255.0, 0, 255)
    res = cv2.LUT(image, lookUpTable)
    return res

img_mod = color_transform(img)
print(img_mod.shape)
img_norm = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
print(img_norm.shape)
plt.subplot(2,2,1),plt.imshow(img_norm, cmap = plt.cm.gray)
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,2),plt.imshow(img_mod, cmap = plt.cm.gray)
plt.title('modified'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,3),plt.imshow(gamma_correction(img_norm,1.6), cmap = plt.cm.gray)
plt.title('gamma'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,4),plt.imshow(gamma_correction(img_mod,1.6), cmap = plt.cm.gray)
plt.title('gamma'), plt.xticks([]), plt.yticks([])
plt.show()

