#https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_gradients/py_gradients.html

import cv2
import numpy as np
import os, os.path
from matplotlib import pyplot as plt
from cv2 import Laplacian, Sobel
import pathlib
from pathlib import Path


# Ordner mit Inhalt Struktur der Daten
data_dir = r"E:\Vorbereitung_testdatensatz"
data_dir = Path(data_dir)  # liest den Pfad ein

label_dir = data_dir
label_names = np.array([item.name for item in label_dir.glob('*')])

for a in label_names:
    print(a)


for item in label_names:

    img = cv2.imread(r"E:/Vorbereitung_testdatensatz/"+ item)

    img = img[470:610, 955:1265]  # aktuell: [490:630, 925:1235]


    # Graustufen
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # img = cv2.cvtColor(img, cv2.COLOR_BGR2BGRA)

    # Bild drehen
    img = cv2.rotate(img, cv2.ROTATE_180)


    def linear_skale(image_bild, min, max):
        img1 = np.zeros(shape=(140, 310), dtype='uint8')
        image = image_bild
        minimum = min
        maximum = max

        #for x in range(0, image.shape[1]):
        #    for y in range(0, image.shape[0]):
        #        print(image[y][x])
        #        if image[y][x] < minimum:
        #                        minimum = image[y][x]
        #        elif image[y][x] > maximum:
        #                        maximum = image[y][x]



        print(minimum, maximum)
        # Grauwerte anpassen
        schwarz = 0
        weiß = 255

        c2 = (weiß - schwarz) / (maximum - minimum)
        c1 = (minimum) * -1


        for x in range(0, image.shape[1]):
            for y in range(0, image.shape[0]):
                if (c2*image[y][x] + c1*c2) < 0:
                    img1[y][x] = 0
                elif (c2*image[y][x] + c1*c2) > 255:
                    img1[y][x] = 255
                else: img1[y][x] = (image[y][x] + c1)*c2

        return img1

    def gamma_correction(image, gamma):
        lookUpTable = np.empty((1, 256), np.uint8)
        for i in range(256):
            lookUpTable[0, i] = np.clip(pow(i / 255.0, gamma) * 255.0, 0, 255)
        res = cv2.LUT(image, lookUpTable)
        return res
    i = 1

    def color_transform(image):
        for x in range(0, image.shape[1]):
            for y in range(0, image.shape[0]):
                for z in range (0,image.shape[2]):

                    summe = summe + image[y][x][z]
                    if (i<2):
                        r = image[y][x][z]
                    if (i==2):
                        g =  image[y][x][z]
                    if (i>3):
                        b = image[y][x][z]

                    i = i +1

            r_update = (r/r+g+b)
            g_update = (g/r+g+b)
            b_update = (b/r+g+b)

            image[y][x] = r_update*r + g_update*g + b_update*b


    img_scale = linear_skale(img,20,180)
    img_scale_1 = linear_skale(img, 20, 255)
    # vorher (img,42,162)
    img_gamma = gamma_correction(img,1)
    img_gamma_1 = gamma_correction(img,2)

    hist1 = cv2.calcHist([img],[0],None,[256],[0,256])
    hist2 = cv2.calcHist([img_scale],[0],None,[256],[0,256])
    hist3 = cv2.calcHist([img_gamma],[0],None,[256],[0,256])
    hist4 = cv2.calcHist([img_gamma_1],[0],None,[256],[0,256])
    hist5 = cv2.calcHist([img_scale_1], [0], None, [256], [0, 256])
    #hist4 = cv2.calcHist([img_rescale_3],[0],None,[256],[0,256])
    plt.plot(hist1)
    plt.title('Original')
    plt.show()

    plt.plot(hist2)
    plt.title('scale_20_180')
    plt.show()

    plt.plot(hist5)
    plt.title('scale_20_250')
    plt.show()
    '''


    plt.plot(hist1)
    plt.show()
    plt.plot(hist2)
    plt.show()
    plt.plot(hist3)
    plt.show()
    plt.plot(hist4)
    plt.show()

    plt.subplot(2,2,1),plt.imshow(img , cmap = plt.cm.gray)
    plt.title('Original'), plt.xticks([]), plt.yticks([])
    plt.subplot(2,2,2),plt.imshow(img_scale , cmap = plt.cm.gray)
    plt.title('linear_scale'), plt.xticks([]), plt.yticks([])
    plt.subplot(2,2,3),plt.imshow(img_gamma , cmap = plt.cm.gray)
    plt.title('gamma_1.5'), plt.xticks([]), plt.yticks([])
    plt.subplot(2,2,4),plt.imshow(img_gamma_1, cmap = plt.cm.gray)
    plt.title('gamma_2'), plt.xticks([]), plt.yticks([])
    plt.show()
    '''

    print(item)
    os.chdir(r"E:/Vorbereitung_testdatensatz/")
    #cv2.imwrite("Ueberarbeitet_gamma_2_" + item, img_gamma_1)
    #cv2.imwrite("Ueberarbeitet_gamma_1_" + item, img_gamma)
    #cv2.imwrite("Ueberarbeitet_linscale_20_180_" + item, img_scale)
    #cv2.imwrite("Ueberarbeitet_normal_" + item, img)
    #cv2.imwrite("Ueberarbeitet_gamma_2_" + item, hist4)
    #cv2.imwrite("Ueberarbeitet_gamma_0.4_" + item, hist3)
    #cv2.imwrite("Ueberarbeitet_linscale_" + item, hist2)



'''
laplacian = cv2.Laplacian(img,cv2.CV_64F)
canny = cv2.Canny(img,100,200)
sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=5)
sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=5)

imgboth = cv2.addWeighted(sobelx, 0.5, sobely, 0.5, 0)
'''
'''
plt.subplot(2,2,1),plt.imshow(img , cmap = plt.cm.gray)
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,2),plt.imshow(laplacian, cmap = plt.cm.gray)
plt.title('Laplacian'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,3),plt.imshow(canny, cmap = plt.cm.gray)
plt.title('Canny'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,4),plt.imshow(imgboth, cmap = plt.cm.gray)
plt.title('Combination'), plt.xticks([]), plt.yticks([])

plt.show()
'''
