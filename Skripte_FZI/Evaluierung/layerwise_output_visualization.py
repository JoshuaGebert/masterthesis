#importing required libraries and functions
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
from keras.models import Model

from keras.preprocessing.image import load_img
# load an image from file


def iter_occlusion(image, size=8):

    occlusion = np.full((size * 5, size * 5, 1), [0.5], np.float32)
    occlusion_center = np.full((size, size, 1), [0.5], np.float32)
    occlusion_padding = size * 2

    # print('padding...')
    image_padded = np.pad(image, ( \
                        (occlusion_padding, occlusion_padding), (occlusion_padding, occlusion_padding), (0, 0) \
                        ), 'constant', constant_values = 0.0)

    for y in range(occlusion_padding, image.shape[0] + occlusion_padding, size):

        for x in range(occlusion_padding, image.shape[1] + occlusion_padding, size):
            tmp = image_padded.copy()

            tmp[y - occlusion_padding:y + occlusion_center.shape[0] + occlusion_padding, \
                x - occlusion_padding:x + occlusion_center.shape[1] + occlusion_padding] \
                = occlusion

            tmp[y:y + occlusion_center.shape[0], x:x + occlusion_center.shape[1]] = occlusion_center

            yield x - occlusion_padding, y - occlusion_padding, \
                  tmp[occlusion_padding:tmp.shape[0] - occlusion_padding, occlusion_padding:tmp.shape[1] - occlusion_padding]



file_path = r"C:\Users\User\masterthesis\data\Datensatz_Klassifikation_balanced\train\labels\Spaghetti\Wuerfel_Spaghetti\Grauw_angegl_335.png"
image_camera = tf.io.read_file(file_path)
image_camera = tf.image.decode_png(image_camera, channels=1)
image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
image_camera = tf.image.resize(image_camera, [140, 310])
plt.title('ORIGINAL IMAGE')

model = tf.keras.models.load_model(
    r'C:\Users\User\masterthesis\training_output_klassifikation\Modell_speicher\nocad')

model.summary()

print("success")


# predict the probability across all output classes
yhat = model.predict(image_camera)

print("success2")
temp = image_camera[0]



print(temp.shape)
heatmap = np.zeros((140,310))
correct_class = np.argmax(yhat)
for n,(x,y,image) in enumerate(iter_occlusion(temp,14)):
    heatmap[x:x+14,y:y+14] = model.predict(image.reshape((1, image.shape[0], image.shape[1], image.shape[2])))[0][correct_class]
    print(x,y,n,' - ',image.shape)
heatmap1 = heatmap/heatmap.max()
plt.imshow(heatmap)