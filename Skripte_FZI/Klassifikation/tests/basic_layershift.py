import tensorflow as tf
import os
import matplotlib.pyplot as plt
from keras import datasets, layers, models
import pathlib
import numpy as np
from datetime import datetime
from tensorflow.keras import datasets, layers, models, activations
from pathlib import Path


AUTOTUNE = tf.data.experimental.AUTOTUNE

data_dir = r"r'/fzi/ids/cl691/no_backup/Datensatz_Klassifikation_balanced/layershift/train'"

data_dir = pathlib.Path(data_dir)

# Dataset aus nur einem Bild erstellen
list_ds = tf.data.Dataset.list_files(str(data_dir/'labels/*/*'))
file_path = next(iter(list_ds))
print(file_path)
# Reads an image from a file, decodes it into a dense tensor, and resizes it
# to a fixed shape.
def show(image, label):
  plt.figure()
  plt.imshow(image)
  plt.axis('off')
  plt.title(label.numpy())
  plt.show()


import scipy.ndimage as ndimage

def random_rotate_image(image):
  image = ndimage.rotate(image, np.random.uniform(-10, 10), reshape=False)
  return image

def tf_random_rotate_image(image, label):
  im_shape = image.shape
  [image,] = tf.py_function(random_rotate_image, [image], [tf.float32])
  image.set_shape(im_shape)
  return image, label

def get_label(filename):
    parts = tf.strings.split(filename, os.path.sep)


    def gut(): return tf.constant(0)
    def layershift(): return tf.constant(1)
    def clogged_nozzle(): return tf.constant(1)
    def spaghetti(): return tf.constant(1)
    def warping(): return tf.constant(1)

    label = tf.case([(parts[-2] == tf.constant('gut'), gut),
                     (parts[-2] == tf.constant('layershift'), warping),
                     (parts[-2] == tf.constant('clogged_nozzle'), clogged_nozzle),
                     (parts[-2] == tf.constant('spaghetti'), spaghetti),
                     (parts[-2] == tf.constant('warping'), spaghetti)
                     ] )

    return label

def parse_image(filename):

    label = get_label(filename)
    image = tf.io.read_file(filename)
    image = tf.image.decode_png(image, channels=1)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize(image, [140, 310])

    return  image, label

# Datensatz erstellen
labeled_ds = list_ds.map(parse_image, num_parallel_calls=AUTOTUNE)
rot_ds = labeled_ds.map(tf_random_rotate_image, num_parallel_calls=AUTOTUNE)
complete_ds = labeled_ds.concatenate(rot_ds)


complete_ds = complete_ds.shuffle(tf.data.experimental.cardinality(complete_ds).numpy(), reshuffle_each_iteration=True)

file_path = next(iter(list_ds))

# Datensatz in Trainigs-, Validierungs-, Testdaten teilen
DATASET_SIZE = tf.data.experimental.cardinality(complete_ds).numpy()
train_size = int(0.8 * DATASET_SIZE)
val_size = int(0.2 * DATASET_SIZE)

train_ds = complete_ds.take(train_size)
val_ds = complete_ds.skip(train_size)
#(train_images, train_labels) = train_ds
#(val_images, val_labels) = val_ds

# Normalize pixel values to be between 0 and 1
#(train_images, test_images) = train_images / 255.0, val_images / 255.0


# Datensatz in Batches aufteilen
BATCH_SIZE = 32
train_ds = train_ds.batch(BATCH_SIZE)
val_ds = val_ds.batch(16)
# input size
img_heigth = 140
img_width = 310

input = tf.keras.Input(shape=(img_heigth, img_width, 1))

x = layers.Conv2D(32, (3, 3), activation='relu', input_shape=(140, 310, 1))(input)
x = tf.keras.layers.BatchNormalization()(x)
x = layers.MaxPooling2D((2,2))(x)
x = layers.Conv2D(64, (3, 3), activation='relu')(x)
x = tf.keras.layers.BatchNormalization()(x)
x = layers.MaxPooling2D((2,2))(x)
x = layers.Conv2D(64, (3, 3), activation='relu')(x)
x = tf.keras.layers.BatchNormalization()(x)
x = layers.Flatten()(x)
x = layers.Dense(64, activation='relu')(x)

output = layers.Dense(1, activation='softmax')(x)

model = tf.keras.Model(inputs=input, outputs=output)
model.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=0.01),
    loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
    metrics=['accuracy']
)

model.summary()


# Model Plot
# tf.keras.utils.plot_model(model, to_file='model_aktuell.png', show_shapes=True)
model.summary()

# Tensorboard callback
log_dir = r'/fzi/ids/cl691/no_backup/Training_output_Klassifikation/Tensorboard/Date' + datetime.now().strftime(
    "%Y%m%d-%H%M%S") + 'binary-3Conv-Adam-0.01-#-#-32-64-batch-32'
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=2)

# Training + Validiierung
model.fit(train_ds, epochs=100, validation_data=val_ds,
          callbacks=[tensorboard_callback])

# komplettes Modell speichern und aus diesem Skript loeschen
model.save('r/fzi/ids/cl691/no_backup/Training_output_Klassifikation/Modell_speicher/binary/basic', overwrite=True)

del model



