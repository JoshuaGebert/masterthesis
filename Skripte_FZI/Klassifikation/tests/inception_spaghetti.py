import tensorflow as tf
import os
import matplotlib.pyplot as plt
from keras import datasets, layers, models
import pathlib
import numpy as np
from datetime import datetime
from tensorflow.keras import datasets, layers, models, activations
from pathlib import Path



AUTOTUNE = tf.data.experimental.AUTOTUNE

data_dir = r"/fzi/ids/cl691/trainingsdatensaetze/Datensatz_Spaghetti"

data_dir = pathlib.Path(data_dir)

# Dataset aus nur einem Bild erstellen

list_ds = tf.data.Dataset.list_files(str(data_dir/'*/*/*'), shuffle=True)
file_path = next(iter(list_ds))
print(file_path)
# Reads an image from a file, decodes it into a dense tensor, and resizes it
# to a fixed shape.


def show(image, label):
  plt.figure()
  plt.imshow(image)
  plt.axis('off')
  plt.title(label.numpy())
  plt.show()


import scipy.ndimage as ndimage

def random_rotate_image(image):
  image = ndimage.rotate(image, np.random.uniform(-5, 5), reshape=False)
  return image

def tf_random_rotate_image(image, label):
  im_shape = image.shape
  [image,] = tf.py_function(random_rotate_image, [image], [tf.float32])
  image.set_shape(im_shape)
  return image, label

def get_label(filename):
    parts = tf.strings.split(filename, os.path.sep)


    def gut(): return tf.constant(0)
    def layershift(): return tf.constant(1)
    def clogged_nozzle(): return tf.constant(1)
    def spaghetti(): return tf.constant(1)
    def warping(): return tf.constant(1)

    label = tf.case([(parts[-3] == tf.constant('gut'), gut),
                     (parts[-3] == tf.constant('layershift'), warping),
                     (parts[-3] == tf.constant('clogged_nozzle'), clogged_nozzle),
                     (parts[-3] == tf.constant('spaghetti'), spaghetti),
                     (parts[-3] == tf.constant('warping'), spaghetti)
                     ] )

    return label

def parse_image(filename):

    label = get_label(filename)
    image = tf.io.read_file(filename)
    image = tf.image.decode_png(image, channels=1)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize(image, [140, 310])

    return  image, label

@tf.function
def leakyrelu(z, alpha):
    test = max(alpha * z, z)
    test = tf.cast(test, tf.float32)
    return test


@tf.function
def my_leaky_relu(x):
    return tf.nn.leaky_relu(x, alpha=0.01)


# Datensatz erstellen
labeled_ds = list_ds.map(parse_image, num_parallel_calls=AUTOTUNE)
rot_ds = labeled_ds.map(tf_random_rotate_image, num_parallel_calls=AUTOTUNE)
complete_ds = labeled_ds.concatenate(rot_ds)

# Datensatz in Trainigs-, Validierungs-, Testdaten teilen
DATASET_SIZE = tf.data.experimental.cardinality(complete_ds).numpy()
train_size = int(0.8 * DATASET_SIZE)
val_size = int(0.2 * DATASET_SIZE)

train_ds = complete_ds.take(train_size)
val_ds = complete_ds.skip(train_size)

# Datensatz in Batches aufteilen
BATCH_SIZE = 32
train_ds = train_ds.batch(BATCH_SIZE)
val_ds = val_ds.batch(16)



# input size
img_heigth = 140
img_width = 310

############################## Netz ##############################

input = layers.Input(shape=(img_heigth, img_width, 1))


### 1st layer
layer_1 = layers.Conv2D(10, (1,1), padding='same', activation='relu')(input)
layer_1 = layers.Conv2D(10, (3,3), padding='same', activation='relu')(layer_1)

layer_2 = layers.Conv2D(10, (1,1), padding='same', activation='relu')(input)
layer_2 = layers.Conv2D(10, (5,5), padding='same', activation='relu')(layer_2)

layer_3 = layers.MaxPooling2D((3,3), strides=(1,1), padding='same')(input)
layer_3 = layers.Conv2D(10, (1,1), padding='same', activation='relu')(layer_3)

mid_1 = layers.concatenate([layer_1, layer_2, layer_3], axis = 3)
### 2st layer

layer_4 = layers.Conv2D(10, (1,1), padding='same', activation='relu')(mid_1)
layer_4 = layers.Conv2D(10, (3,3), padding='same', activation='relu')(layer_4)

layer_5 = layers.Conv2D(10, (1,1), padding='same', activation='relu')(mid_1)
layer_5 = layers.Conv2D(10, (5,5), padding='same', activation='relu')(layer_5)

layer_6 = layers.MaxPooling2D((3,3), strides=(1,1), padding='same')(mid_1)
layer_6 = layers.Conv2D(10, (1,1), padding='same', activation='relu')(layer_6)

mid_2 = layers.concatenate([layer_4, layer_5, layer_5], axis = 3)

flat_1 = layers.Flatten()(mid_2)

x = layers.Dense(1200, activation='relu')(flat_1)
x = layers.Dense(600, activation='relu')(x)
x = layers.Dense(150, activation='relu')(x)

#output
output = layers.Dense(1, activation="softmax")(x)

############################## Netz ##############################

model = tf.keras.Model(inputs=input, outputs=output)
model.compile( optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
      loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
     metrics=['accuracy'])


model.summary()

tf.plot_model(model, to_file='model.png', show_shapes=True, show_layer_names=True)


model.summary()

log_dir = r'/fzi/ids/cl691/Training_output_Klassifikation/Tensorboard/Date' + datetime.now().strftime(
    "%Y%m%d-%H%M%S") + 'inception-spaghetti'
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=2)

model.fit(train_ds, epochs=100, validation_data=val_ds, callbacks=[tensorboard_callback])

# komplettes Modell speichern und aus diesem Skript loeschen
model.save(r'/fzi/ids/cl691/Training_output_Klassifikation/Modell_speicher/binary/inception', overwrite=True)
del model

