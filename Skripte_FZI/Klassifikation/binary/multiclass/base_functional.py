import tensorflow as tf
import numpy as np
import keras
import matplotlib.pyplot as plt
import os
import cv2
from tensorflow.keras.models import Sequential
import pathlib
from pathlib import Path
AUTOTUNE = tf.data.experimental.AUTOTUNE
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D, Input, GlobalAveragePooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import pickle
import random
from datetime import datetime

datadir = r"/fzi/ids/cl691/trainingsdatensaetze/Wuerfel_Grundkoerper"


data_dir = pathlib.Path(datadir)

list_ds = tf.data.Dataset.list_files([str(data_dir/'*/*')],  shuffle=True)

def get_label(file_path):

  parts = tf.strings.split(file_path, os.path.sep)


  def gut(): return tf.constant(0)
  def layershift(): return tf.constant(2)
  def clogged_nozzle(): return tf.constant(4)
  def spaghetti(): return tf.constant(3)
  def warping(): return tf.constant(1)

  label = tf.case([(parts[-2] == tf.constant('gut'), gut),
                   (parts[-2] == tf.constant('layershift'), layershift),
                   (parts[-2] == tf.constant('clogged_nozzle'), clogged_nozzle),
                   (parts[-2] == tf.constant('spaghetti'), spaghetti),
                   (parts[-2] == tf.constant('warping'), warping)
                   ])

  return label


def load_images(file_path):
    # Label einlesen
    label = get_label(file_path)

    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])

    return image_camera, label

def augmentation(file_path):
    # Label einlesen
    label = get_label(file_path)
    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])
    image_camera = tf.image.random_flip_left_right(image_camera)
    image_camera = tf.image.random_brightness(image_camera, max_delta=0.2)

    return image_camera, label



labeled_ds = list_ds.map(load_images, num_parallel_calls=AUTOTUNE).cache()
augmentet_ds = list_ds.map(augmentation, num_parallel_calls=AUTOTUNE)
complete_ds = labeled_ds.concatenate(augmentet_ds)
#complete_ds = labeled_ds
# .shuffle(tf.data.experimental.cardinality(labeled_ds).numpy(), reshuffle_each_iteration=True)

# Dataset in Trainigs-, Validierungs-, Testdaten teilen
DATASET_SIZE = tf.data.experimental.cardinality(complete_ds).numpy()
train_size = int(0.8 * DATASET_SIZE)
val_size = int(0.2 * DATASET_SIZE)

train_ds = complete_ds.take(train_size)
val_ds = complete_ds.skip(train_size)

# Datasets in Batches aufteilen
BATCH_SIZE = 32
train_ds = train_ds.batch(BATCH_SIZE)
val_ds = val_ds.batch (32)

img_heigth = 140
img_width = 310

############################## Netz ##############################

input = Input(shape=(img_heigth, img_width, 1))

x = Conv2D(64, (4, 4), activation='relu',  input_shape=(140, 310, 1))(input)
x = MaxPooling2D((2,2))(x)
x = Dropout(0.4)(x)
x = Conv2D(64, (4, 4), activation='relu' )(x)
x = Conv2D(64, (3, 3), activation='relu')(x)
x = Conv2D(64, (4, 4), activation='relu')(x)
x = MaxPooling2D((2,2))(x)
x = Dropout(0.4)(x)
x = Conv2D(128, (4, 4), activation='relu')(x)
x = Conv2D(128, (3, 3), activation='relu')(x)
x = Conv2D(128, (4, 4), activation='relu')(x)
x = MaxPooling2D((2,2))(x)
x = Dropout(0.4)(x)
x = Conv2D(256, (4, 4), activation='relu')(x)
x = MaxPooling2D((2,2))(x)
x = Dropout(0.4)(x)


# x = Flatten()(x)
x = Dense(128, activation='relu')(x)
x = Dropout(0.4)(x)
x = Dense(128, activation='relu')(x)
x = Dropout(0.4)(x)
x = Dense(128, activation='relu')(x)

x = GlobalAveragePooling2D()(x)

output = Dense(5, activation='softmax')(x)

############################## Netz ##############################

model = tf.keras.Model(inputs=input, outputs=output)

model.compile(loss="sparse_categorical_crossentropy",
              optimizer="adam",
              metrics=["accuracy"])


model.summary()

log_dir = "/fzi/ids/cl691/Training_output_Klassifikation/Tensorboard/multiclass/date_" + datetime.now().strftime("%Y%m%d-%H%M%S") + "_vgg-like_mehrklassen_alldata_"
#log_dir = "F:\Tensorboard\date_" + datetime.now().strftime("%Y%m%d-%H%M%S") \
#         + "_mehrklassen_wuerfel_all_functional-base"

early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=5, restore_best_weights=True)
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

model.fit(train_ds,  epochs=30, validation_data=val_ds, callbacks=[tensorboard_callback, early_stopping])

model.save(r"/fzi/ids/cl691/Training_output_Klassifikation/Modell_speicher/multiclass/vgg_like_es")


