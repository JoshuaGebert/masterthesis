import tensorflow as tf
import numpy as np
import keras
import matplotlib.pyplot as plt
import os
import cv2
from tensorflow.keras.models import Sequential
import pathlib
from pathlib import Path
AUTOTUNE = tf.data.experimental.AUTOTUNE
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D, Input, GlobalAveragePooling2D, concatenate
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import pickle
import random
from datetime import datetime

datadir = r"/fzi/ids/cl691/trainingsdatensaetze/Wuerfel_Grundkoerper"


data_dir = pathlib.Path(datadir)

list_ds = tf.data.Dataset.list_files([str(data_dir/'*/*')],  shuffle=True)

def get_label(file_path):

  parts = tf.strings.split(file_path, os.path.sep)


  def gut(): return tf.constant(0)
  def layershift(): return tf.constant(2)
  def clogged_nozzle(): return tf.constant(4)
  def spaghetti(): return tf.constant(3)
  def warping(): return tf.constant(1)

  label = tf.case([(parts[-2] == tf.constant('gut'), gut),
                   (parts[-2] == tf.constant('layershift'), layershift),
                   (parts[-2] == tf.constant('clogged_nozzle'), clogged_nozzle),
                   (parts[-2] == tf.constant('spaghetti'), spaghetti),
                   (parts[-2] == tf.constant('warping'), warping)
                   ])

  return label


def load_images(file_path):
    # Label einlesen
    label = get_label(file_path)

    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])

    return image_camera, label

def augmentation(file_path):
    # Label einlesen
    label = get_label(file_path)
    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])
    image_camera = tf.image.random_flip_left_right(image_camera)
    image_camera = tf.image.random_brightness(image_camera, max_delta=0.2)

    return image_camera, label



labeled_ds = list_ds.map(load_images, num_parallel_calls=AUTOTUNE).cache()
augmentet_ds = list_ds.map(augmentation, num_parallel_calls=AUTOTUNE)
complete_ds = labeled_ds.concatenate(augmentet_ds)
#complete_ds = labeled_ds
# .shuffle(tf.data.experimental.cardinality(labeled_ds).numpy(), reshuffle_each_iteration=True)

# Dataset in Trainigs-, Validierungs-, Testdaten teilen
DATASET_SIZE = tf.data.experimental.cardinality(complete_ds).numpy()
train_size = int(0.8 * DATASET_SIZE)
val_size = int(0.2 * DATASET_SIZE)

train_ds = complete_ds.take(train_size)
val_ds = complete_ds.skip(train_size)

# Datasets in Batches aufteilen
BATCH_SIZE = 32
train_ds = train_ds.batch(BATCH_SIZE)
val_ds = val_ds.batch (32)

img_heigth = 140
img_width = 310

############################## Netz ##############################
input = Input(shape=(img_heigth, img_width, 1))

x = Conv2D(filters=64,kernel_size=3, activation='relu')(input)
x = Conv2D(filters=64,kernel_size=3, activation='relu')(x)
x = MaxPooling2D((2, 2))(x)

### 1st layer
layer_1 = Conv2D(24, (1,1), padding='same', activation='relu')(x)
layer_1 = Conv2D(24, (3,3), padding='same', activation='relu')(layer_1)

layer_2 = Conv2D(24, (1,1), padding='same', activation='relu')(x)
layer_2 = Conv2D(24, (5,5), padding='same', activation='relu')(layer_2)

layer_3 = MaxPooling2D((3,3), strides=(1,1), padding='same')(x)
layer_3 = Conv2D(24, (1,1), padding='same', activation='relu')(layer_3)

mid_1 = concatenate([layer_1, layer_2, layer_3], axis = 3)
### 2st layer

layer_4 = Conv2D(24, (1,1), padding='same', activation='relu')(mid_1)
layer_4 = Conv2D(24, (3,3), padding='same', activation='relu')(layer_4)

layer_5 = Conv2D(24, (1,1), padding='same', activation='relu')(mid_1)
layer_5 = Conv2D(24, (5,5), padding='same', activation='relu')(layer_5)

layer_6 = MaxPooling2D((3,3), strides=(1,1), padding='same')(mid_1)
layer_6 = Conv2D(24, (1,1), padding='same', activation='relu')(layer_6)

mid_2 = concatenate([layer_4, layer_5, layer_5], axis = 3)
x = MaxPooling2D((2, 2))(mid_2)
### 3rd layer

layer_7 = Conv2D(24, (1,1), padding='same', activation='relu')(x)
layer_7 = Conv2D(24, (3,3), padding='same', activation='relu')(layer_7)

layer_8 = Conv2D(24, (1,1), padding='same', activation='relu')(x)
layer_8 = Conv2D(24, (5,5), padding='same', activation='relu')(layer_8)

layer_9 = MaxPooling2D((3,3), strides=(1,1), padding='same')(x)
layer_9 = Conv2D(24, (1,1), padding='same', activation='relu')(layer_9)

mid_3 = concatenate([layer_7, layer_8, layer_9], axis = 3)

### 4rd layer

layer_10 = Conv2D(24, (1,1), padding='same', activation='relu')(mid_3)
layer_10 = Conv2D(24, (3,3), padding='same', activation='relu')(layer_10)

layer_11 = Conv2D(24, (1,1), padding='same', activation='relu')(mid_3)
layer_11 = Conv2D(24, (5,5), padding='same', activation='relu')(layer_11)

layer_12 = MaxPooling2D((3,3), strides=(1,1), padding='same')(mid_3)
layer_12 = Conv2D(24, (1,1), padding='same', activation='relu')(layer_12)

mid_4 = concatenate([layer_10, layer_11, layer_12], axis = 3)


x = Dense(128, activation='relu')(mid_4)
x = Dropout(0.4)(x)

glob = GlobalAveragePooling2D()(x)

#output
output = Dense(5, activation="softmax")(glob)

model = tf.keras.Model(inputs=input, outputs=output)

model.compile(loss="sparse_categorical_crossentropy",
              optimizer="adam",
              metrics=["accuracy"])


model.summary()

log_dir = "/fzi/ids/cl691/Training_output_Klassifikation/Tensorboard/multiclass/date_" + datetime.now().strftime("%Y%m%d-%H%M%S") + "_inmception-like_po_mehrklassen_alldata_"
#log_dir = "F:\Tensorboard\date_" + datetime.now().strftime("%Y%m%d-%H%M%S") \
#         + "_mehrklassen_wuerfel_all_functional-base"
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

model.fit(train_ds,  epochs=30, validation_data=val_ds, callbacks=[tensorboard_callback])

model.save(r"/fzi/ids/cl691/Training_output_Klassifikation/Modell_speicher/multiclass/inception_like_po")


