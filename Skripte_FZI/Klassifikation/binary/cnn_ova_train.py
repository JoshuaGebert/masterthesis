import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Input,GlobalAveragePooling2D,Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D, BatchNormalization
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import pickle
import random
from datetime import datetime

datadir = r"/fzi/ids/cl691/trainingsdatensaetze/ova/binary_gut"
categories = ["no_gut", "gut"]
IMG_height = 140
IMG_width = 310


training_data = []

def create_training_data():
    for category in categories:
        path = os.path.join(datadir, category)
        class_num =categories.index(category)
        print(path)
        for img in os.listdir(path):
            img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
            new_array = cv2.resize(img_array, (IMG_width, IMG_height))
            training_data.append([new_array,class_num])


create_training_data()

print(len(training_data))

random.shuffle(training_data)

X = []
y = []

for features, label in training_data:
    X.append(features)
    y.append(label)

X = np.array(X).reshape(-1, IMG_height, IMG_width, 1)
y = np.array(y)


X = X/255.0


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


img_heigth = 140
img_width = 310


model = Sequential()

model.add(Conv2D(64,(3,3), input_shape = X.shape[1:]))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(64, (3,3)))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(64, (3,3)))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(64, (3,3)))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2,2)))


# model.add(Flatten())
model.add(GlobalAveragePooling2D())
model.add(Dense(64))



model.add(Dense(1))
model.add(Activation("sigmoid"))

model.compile(loss="binary_crossentropy",
              optimizer="adam",
              metrics=["accuracy"])
model.summary()

# tensorboard inititation
log_dir = "/fzi/ids/cl691/Training_output_Klassifikation/Tensorboard/start/date_" + datetime.now().strftime("%Y%m%d-%H%M%S") + "ova_gut-4cnn"

tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

model.fit(X_train, y_train, batch_size=32, epochs=30, validation_split=0.1, callbacks=[tensorboard_callback])

model.save(r"/fzi/ids/cl691/Training_output_Klassifikation/Modell_speicher/binary/4cnn/gut")

y_pred = model.predict_classes(X_test)
# y_pred = (model.predict(X_test) > 0.5).astype("int32")
cm = confusion_matrix(y_test, y_pred)
print(cm)


fig1 = plt.figure(figsize=(20,20))

for i in range(10):
    n = random.randint(1, 978)
    fig1.add_subplot(2,5,i+1)
    plt.title(y_pred[n])
    plt.imshow(X_test[n])
    plt.imshow(X_test[n], cmap='gray')

plt.show()








