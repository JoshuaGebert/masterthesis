# https://www.tensorflow.org/guide/data#preprocessing_data

from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf
from keras import datasets, layers, models
import numpy as np
import matplotlib.pyplot as plt
import os
import pathlib
from pathlib import Path
from datetime import datetime




AUTOTUNE = tf.data.experimental.AUTOTUNE
direct_name = r'/fzi/ids/cl691/no_backup/Datensatz_Klassifikation_balanced/train'
direct_name = pathlib.Path(direct_name)

list_ds = tf.data.Dataset.list_files(str(direct_name/'labels/*/*/*'))

# Reads an image from a file, decodes it into a dense tensor, and resizes it
# to a fixed shape.

def get_label(filename):
    parts = tf.strings.split(filename, os.path.sep)

    def gut(): return tf.constant(0)
    def layershift(): return tf.constant(1)
    def clogged_nozzle(): return tf.constant(2)
    def spaghetti(): return tf.constant(3)

    label = tf.case([(parts[-3] == tf.constant('gut'), gut),
                     (parts[-3] == tf.constant('Layershift'), layershift),
                     (parts[-3] == tf.constant('Clogged_Nozzle'), clogged_nozzle),
                     (parts[-3] == tf.constant('Spaghetti'), spaghetti)
                     ])
    return label

def parse_image(filename):

  label = get_label(filename)

  image = tf.io.read_file(filename)
  image = tf.image.decode_png(image, channels=1)
  image = tf.image.convert_image_dtype(image, tf.float32)
  image = tf.image.resize(image, [140, 310])

  return image, label

# Datensatz erstellen
labeled_ds = list_ds.map(parse_image, num_parallel_calls=AUTOTUNE)

complete_ds = labeled_ds.shuffle(tf.data.experimental.cardinality(labeled_ds).numpy(), reshuffle_each_iteration=True)

# Datensatz in Trainigs-, Validierungs-, Testdaten teilen
DATASET_SIZE = tf.data.experimental.cardinality(complete_ds).numpy()
train_size = int(0.8 * DATASET_SIZE)
val_size = int(0.2 * DATASET_SIZE)

train_ds = complete_ds.take(train_size)
val_ds = complete_ds.skip(train_size)

# Datensatz in Batches aufteilen
BATCH_SIZE = 32
train_ds = train_ds.batch(BATCH_SIZE)
val_ds = val_ds.batch(16)



############################## Netz ##############################
img_heigth = 140
img_width = 310

# Inputlayers
cam_input = tf.keras.Input(shape=(img_heigth, img_width, 1), name='camera_image')


x = tf.keras.layers.Conv2D(filters=10, kernel_size=3, strides=(1, 1), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(cam_input)
x = tf.keras.layers.BatchNormalization()(x)
x = tf.keras.layers.Conv2D(filters=10, kernel_size=3, strides=(1, 1), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(x)
x = tf.keras.layers.BatchNormalization()(x)
x = tf.keras.layers.MaxPool2D(pool_size=(2, 2))(x)
x = tf.keras.layers.Dropout(0.25)(x)

x = tf.keras.layers.Conv2D(filters=20, kernel_size=3, strides=(1, 1), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(x)
x = tf.keras.layers.BatchNormalization()(x)
x = tf.keras.layers.Conv2D(filters=20, kernel_size=3, strides=(1, 1), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(x)
x = tf.keras.layers.BatchNormalization()(x)
x = tf.keras.layers.MaxPool2D(pool_size=(2, 2))(x)
x = tf.keras.layers.Dropout(0.25)(x)

x = tf.keras.layers.Conv2D(filters=40, kernel_size=3, strides=(2, 2), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(x)
x = tf.keras.layers.BatchNormalization()(x)
x = tf.keras.layers.Conv2D(filters=40, kernel_size=3, strides=(2, 2), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(x)
x = tf.keras.layers.BatchNormalization()(x)
x = tf.keras.layers.MaxPool2D(pool_size=(2, 2))(x)
x = tf.keras.layers.Dropout(0.25)(x)

x = tf.keras.layers.Conv2D(filters=80, kernel_size=3, strides=(2, 2), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(x)
x = tf.keras.layers.BatchNormalization()(x)
x = tf.keras.layers.Conv2D(filters=80, kernel_size=3, strides=(2, 2), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(x)
x = tf.keras.layers.Dropout(0.25)(x)

x = tf.keras.layers.Flatten()(x)
x = tf.keras.layers.Dense(240, activation='elu')(x)
x = tf.keras.layers.Dropout(0.5)(x)

# output
output = tf.keras.layers.Dense(4, activation='softmax')(x)

# Modell bauen
model = tf.keras.Model(inputs=cam_input, outputs=output)

model.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=['accuracy']
)

# Model Plot
# tf.keras.utils.plot_model(model, to_file='model_aktuell.png', show_shapes=True)
model.summary()

# Tensorboard callback
log_dir = r'/fzi/ids/cl691/no_backup/Training_output_Klassifikation/Tensorboard/Date' + datetime.now().strftime(
    "%Y%m%d-%H%M%S") + 'monolithic-jonas-8Conv-Adam-0.001-l2-0.001-10-20-40-80-batch-32'
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=2)

# Early Stopping
early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=8, restore_best_weights=True)

# Training + Validiierung
model.fit(train_ds, epochs=30, validation_data=val_ds, callbacks=[tensorboard_callback, early_stopping])

# komplettes Modell speichern und aus diesem Skript loeschen
model.save(r'/fzi/ids/cl691/no_backup/Training_output_Klassifikation/Modell_speicher/monolithic/jonas', overwrite=True)
del model
