import tensorflow as tf
import os
import matplotlib.pyplot as plt
import pathlib
from datetime import datetime
from tensorflow.keras import datasets, layers, models, activations
from pathlib import Path



AUTOTUNE = tf.data.experimental.AUTOTUNE
direct_name = r'/fzi/ids/cl691/no_backup/Datensatz_Klassifikation_balanced/train'
direct_name = pathlib.Path(direct_name)

list_ds = tf.data.Dataset.list_files(str(direct_name/'labels/*/*/*'))

# Reads an image from a file, decodes it into a dense tensor, and resizes it
# to a fixed shape.
def get_label(filename):
    parts = tf.strings.split(filename, os.path.sep)


    def gut(): return tf.constant(0)
    def layershift(): return tf.constant(1)
    def clogged_nozzle(): return tf.constant(2)
    def spaghetti(): return tf.constant(3)

    label = tf.case([(parts[-3] == tf.constant('gut'), gut),
                     (parts[-3] == tf.constant('Layershift'), layershift),
                     (parts[-3] == tf.constant('Clogged_Nozzle'), clogged_nozzle),
                     (parts[-3] == tf.constant('Spaghetti'), spaghetti)
                     ])
    return label




def parse_image(filename):

  label = get_label(filename)

  image = tf.io.read_file(filename)
  image = tf.image.decode_png(image, channels=1)
  image = tf.image.convert_image_dtype(image, tf.float32)
  image = tf.image.resize(image, [140, 310])

  return image, label

file_path = next(iter(list_ds))
image, label = parse_image(file_path)
print(image, label)
labeled_ds = list_ds.map(parse_image, num_parallel_calls=AUTOTUNE)
complete_ds = labeled_ds.shuffle(tf.data.experimental.cardinality(labeled_ds).numpy(), reshuffle_each_iteration=True)

# Dataset in Trainigs-, Validierungs-, Testdaten teilen
DATASET_SIZE = tf.data.experimental.cardinality(complete_ds).numpy()
train_size = int(0.8 * DATASET_SIZE)
val_size = int(0.2 * DATASET_SIZE)

train_ds = complete_ds.take(train_size)
val_ds = complete_ds.skip(train_size)

BATCH_SIZE = 32
train_ds = train_ds.batch(BATCH_SIZE)
val_ds = val_ds.batch(1)


############################## Residual Block ##############################

def residual_block(y, nb_channels, _strides=(1, 1), _project_shortcut=False):
    shortcut = y

    # down-sampling is performed with a stride of 2
    y = layers.Conv2D(nb_channels, kernel_size=(3, 3), strides=_strides, padding='same')(y)
    y = layers.BatchNormalization()(y)
    y = layers.LeakyReLU()(y)

    y = layers.Conv2D(nb_channels, kernel_size=(3, 3), strides=(1, 1), padding='same')(y)
    y = layers.BatchNormalization()(y)

    # identity shortcuts used directly when the input and output are of the same dimensions
    if _project_shortcut or _strides != (1, 1):
        # when the dimensions increase projection shortcut is used to match dimensions (done by 1×1 convolutions)
        # when the shortcuts go across feature maps of two sizes, they are performed with a stride of 2
        shortcut = layers.Conv2D(nb_channels, kernel_size=(1, 1), strides=_strides, padding='same')(shortcut)
        shortcut = layers.BatchNormalization()(shortcut)

    y = layers.add([shortcut, y])
    y = layers.LeakyReLU()(y)

    return y

############################## Netz ##############################
img_heigth = 140
img_width = 310

# Inputlayers
cam_input = layers.Input(shape=(img_heigth, img_width, 1), name='camera_image')

x = layers.Conv2D(filters=64, kernel_size=5, strides=(1, 1), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(cam_input)
x = layers.BatchNormalization()(x)
x = layers.Conv2D(filters=64, kernel_size=5, strides=(1, 1), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(x)
x = layers.BatchNormalization()(x)
x = layers.MaxPool2D(pool_size=(2, 2))(x)
x = layers.Dropout(0.25)(x)

x = residual_block(x,128,(1,1),True)

x = layers.Conv2D(filters=128, kernel_size=3, strides=(1, 1), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(x)
x = layers.BatchNormalization()(x)
x = layers.MaxPool2D(pool_size=(2, 2))(x)
x = layers.Dropout(0.25)(x)

x = layers.Conv2D(filters=256, kernel_size=3, strides=(1, 1), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(x)
x = layers.BatchNormalization()(x)

x = residual_block(x,256,(1,1),True)

x = layers.Conv2D(filters=256, kernel_size=3, strides=(1, 1), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(x)
x = layers.BatchNormalization()(x)
x = layers.MaxPool2D(pool_size=(2, 2))(x)
x = layers.Dropout(0.25)(x)

x = layers.Flatten()(x)
x = layers.Dense(240, activation='elu')(x)
x = layers.Dense(240, activation='elu')(x)


output = layers.Dense(4, activation='softmax')(x)


# Modell bauen
model = tf.keras.Model(inputs=cam_input, outputs=output)

model.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=0.01),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=['accuracy']
)

# Model Plot
# tf.keras.utils.plot_model(model, to_file='model_aktuell.png', show_shapes=True)
model.summary()


# Tensorboard callback
log_dir = r'/fzi/ids/cl691/no_backup/Training_output_Klassifikation/Tensorboard/Date' + datetime.now().strftime(
    "%Y%m%d-%H%M%S") + 'residual-5Conv-Adam-0.01-l2-0.001-16-32-48-batch-32'

tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=2)

# Early Stopping
early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=8, restore_best_weights=True)

model.summary()

model.fit(train_ds, epochs=100, validation_data=val_ds, callbacks=[tensorboard_callback, early_stopping])

# komplettes Modell speichern und aus diesem Skript loeschen
model.save(r'/fzi/ids/cl691/no_backup/Training_output_Klassifikation/Modell_speicher/balanced/residual', overwrite=True)
del model

