# https://www.tensorflow.org/guide/data#preprocessing_data

from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf
from tensorflow.keras import datasets, layers, models, activations
import numpy as np
import matplotlib.pyplot as plt
import os
import pathlib
from pathlib import Path
from datetime import datetime

AUTOTUNE = tf.data.experimental.AUTOTUNE

# Ordner mit Inhalt Struktur der Daten
data_dir = r'E:/Trainingsdaten_Master/Trainingsdaten/Trainingsdatensaetze/Datensatz_Klassifikation_balanced/train'
data_dir = pathlib.Path(data_dir)  # liest den Pfad ein

# Labels in Array schreiben
label_dir = data_dir / 'labels'
label_names = np.array([item.name for item in label_dir.glob('*')])

# Dataset aus allen Dateipfaden anlegen
list_ds = tf.data.Dataset.list_files([str(data_dir / 'labels/*/*/*')])


# Label abh. von Kamerabild einlesen.
def get_label(file_path):
    parts = tf.strings.split(file_path, os.path.sep)

    def gut(): return tf.constant(0)

    def layershift(): return tf.constant(1)

    def clogged_nozzle(): return tf.constant(2)

    def spaghetti(): return tf.constant(3)

    label = tf.case([(parts[-3] == tf.constant('gut'), gut),
                     (parts[-3] == tf.constant('Layershift'), layershift),
                     (parts[-3] == tf.constant('Clogged_Nozzle'), clogged_nozzle),
                     (parts[-3] == tf.constant('Spaghetti'), spaghetti)
                     ])

    return label


# set (Kamerabild, CAD_Bild, Label) erstellen
def load_images(file_path):
    # Label einlesen
    label = get_label(file_path)

    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])


    return {'camera_image': image_camera}, {'voll_connected': label}


def augmentation(file_path):
    # Label einlesen
    label = get_label(file_path)

    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])
    image_camera = tf.image.random_brightness(image_camera, max_delta=0.2)

    return {'camera_image': image_camera}, {'voll_connected': label}


labeled_ds = list_ds.map(load_images, num_parallel_calls=AUTOTUNE).cache()
augmentet_ds = list_ds.map(augmentation, num_parallel_calls=AUTOTUNE)
complete_ds = labeled_ds.concatenate(augmentet_ds)

# Dataset shuffle
complete_ds = complete_ds.shuffle(tf.data.experimental.cardinality(complete_ds).numpy(), reshuffle_each_iteration=True)

# Dataset in Trainigs-, Validierungs-, Testdaten teilen
DATASET_SIZE = tf.data.experimental.cardinality(complete_ds).numpy()
train_size = int(0.8 * DATASET_SIZE)
val_size = int(0.2 * DATASET_SIZE)

train_ds = complete_ds.take(train_size)
val_ds = complete_ds.skip(train_size)

# Datasets in Batches aufteilel
BATCH_SIZE = 32
train_ds = train_ds.batch(BATCH_SIZE)
val_ds = val_ds.batch(1)



def residual_block(y, nb_channels, _strides=(1, 1), _project_shortcut=False):
    shortcut = y

    # down-sampling is performed with a stride of 2
    y = layers.Conv2D(nb_channels, kernel_size=(3, 3), strides=_strides, padding='same')(y)
    y = layers.BatchNormalization()(y)
    y = layers.LeakyReLU()(y)

    y = layers.Conv2D(nb_channels, kernel_size=(3, 3), strides=(1, 1), padding='same')(y)
    y = layers.BatchNormalization()(y)

    # identity shortcuts used directly when the input and output are of the same dimensions
    if _project_shortcut or _strides != (1, 1):
        # when the dimensions increase projection shortcut is used to match dimensions (done by 1×1 convolutions)
        # when the shortcuts go across feature maps of two sizes, they are performed with a stride of 2
        shortcut = layers.Conv2D(nb_channels, kernel_size=(1, 1), strides=_strides, padding='same')(shortcut)
        shortcut = layers.BatchNormalization()(shortcut)

    y = layers.add([shortcut, y])
    y = layers.LeakyReLU()(y)

    return y

############################## Netz ##############################
img_heigth = 140
img_width = 310

# Inputlayers
cam_input = layers.Input(shape=(img_heigth, img_width, 1), name='camera_image')

x = layers.Conv2D(filters=10, kernel_size=5, strides=(1, 1), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(cam_input)
x = layers.BatchNormalization()(x)
x = layers.Conv2D(filters=10, kernel_size=5, strides=(1, 1), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(x)
x = layers.BatchNormalization()(x)
x = layers.MaxPool2D(pool_size=(2, 2))(x)
x = layers.Dropout(0.25)(x)

x = residual_block(x,20,(1,1),True)

x = layers.Conv2D(filters=40, kernel_size=3, strides=(1, 1), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(x)
x = layers.BatchNormalization()(x)
x = layers.MaxPool2D(pool_size=(2, 2))(x)
x = layers.Dropout(0.25)(x)

x = layers.Conv2D(filters=40, kernel_size=3, strides=(1, 1), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(x)
x = layers.BatchNormalization()(x)

x = residual_block(x,80,(1,1),True)

x = layers.Conv2D(filters=80, kernel_size=3, strides=(1, 1), padding='same', activation='elu',
                           kernel_regularizer=tf.keras.regularizers.l2(0.001))(x)
x = layers.BatchNormalization()(x)
x = layers.MaxPool2D(pool_size=(2, 2))(x)
x = layers.Dropout(0.25)(x)

x = layers.Flatten()(x)
x = layers.Dense(240, activation='elu')(x)
x = layers.Dense(240, activation='elu')(x)


voll_connected = layers.Dense(4, activation='softmax', name="voll_connected")(x)


# Modell bauen
model = tf.keras.Model(inputs=cam_input, outputs=voll_connected)

model.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=['accuracy']
)

# Model Plot
# tf.keras.utils.plot_model(model, to_file='model_aktuell.png', show_shapes=True)
model.summary()

# Tensorboard callback
log_dir = r'E:\Training_output_Klassifikation\Tensorboard\Date' + datetime.now().strftime(
    "%Y%m%d-%H%M%S")

tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=2)

# Early Stopping
early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=8, restore_best_weights=True)

# Training + Validiierung
model.fit(train_ds, epochs=10, validation_data=val_ds, callbacks=[tensorboard_callback, early_stopping])

# komplettes Modell speichern und aus diesem Skript loeschen
model.save(r'E:/Training_output_Klassifikation/Modell_speicher', overwrite=True)
del model
