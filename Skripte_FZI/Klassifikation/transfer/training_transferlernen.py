from tensorflow.keras.applications.resnet50 import ResNet50
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras import Model
from tensorflow.keras.preprocessing import image_dataset_from_directory
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import datasets, layers, models, activations
import numpy as np
import matplotlib.pyplot as plt
import os
import pathlib
from pathlib import Path
from datetime import datetime

AUTOTUNE = tf.data.experimental.AUTOTUNE

# Ordner mit Inhalt Struktur der Daten
data_dir = r'/fzi/ids/cl691/no_backup/Datensatz_Klassifikation_balanced/train'
data_dir = pathlib.Path(data_dir)  # liest den Pfad ein

# Labels in Array schreiben
label_dir = data_dir / 'labels'
label_names = np.array([item.name for item in label_dir.glob('*')])

# Dataset aus allen Dateipfaden anlegen
list_ds = tf.data.Dataset.list_files([str(data_dir / 'labels/*/*/*')])


def get_label(filename):
    parts = tf.strings.split(filename, os.path.sep)


    def gut(): return tf.constant(0)
    def layershift(): return tf.constant(1)
    def clogged_nozzle(): return tf.constant(2)
    def spaghetti(): return tf.constant(3)

    label = tf.case([(parts[-3] == tf.constant('gut'), gut),
                     (parts[-3] == tf.constant('Layershift'), layershift),
                     (parts[-3] == tf.constant('Clogged_Nozzle'), clogged_nozzle),
                     (parts[-3] == tf.constant('Spaghetti'), spaghetti)
                     ])
    return label




def parse_image(filename):

  label = get_label(filename)

  image = tf.io.read_file(filename)
  image = tf.image.decode_png(image, channels=1)
  image = tf.image.convert_image_dtype(image, tf.float32)
  image = tf.image.resize(image, [140, 310])

  return image, label


file_path = next(iter(list_ds))
image, label = parse_image(file_path)
print(image, label)
labeled_ds = list_ds.map(parse_image, num_parallel_calls=AUTOTUNE)
complete_ds = labeled_ds.shuffle(tf.data.experimental.cardinality(labeled_ds).numpy(), reshuffle_each_iteration=True)

# Dataset in Trainigs-, Validierungs-, Testdaten teilen
DATASET_SIZE = tf.data.experimental.cardinality(complete_ds).numpy()
train_size = int(0.8 * DATASET_SIZE)
val_size = int(0.2 * DATASET_SIZE)

train_ds = complete_ds.take(train_size)
val_ds = complete_ds.skip(train_size)

BATCH_SIZE = 2
train_ds = train_ds.batch(BATCH_SIZE)
val_ds = val_ds.batch(1)


img_heigth = 140
img_width = 310


InputLayer = tf.keras.Input(shape=(img_heigth, img_width, 3), name='camera_image')



resnet_model = ResNet50(include_top=False, input_shape=(140,310,3))

x = resnet_model.layers[-1].output
output = tf.keras.layers.Dense(units=4, activation='softmax')(x)

model = Model(inputs=resnet_model.input, outputs=output)

for layer in model.layers[:-8]:
    layer.trainable = False

model.summary()
input_shape = model.layers[0].output_shape[0][1:3]
print(input_shape)
model.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=['accuracy']
)

# Model Plot
# tf.keras.utils.plot_model(model, to_file='model_aktuell.png', show_shapes=True)
model.summary()

# Tensorboard callback
log_dir = r'/fzi/ids/cl691/no_backup/Training_output_Klassifikation/Tensorboard/Date' + datetime.now().strftime(
    "%Y%m%d-%H%M%S")

tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=2)

# Early Stopping
early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=8, restore_best_weights=True)

# Training + Validiierung
model.fit(train_ds, epochs=10, validation_data=val_ds, callbacks=[tensorboard_callback, early_stopping])

# komplettes Modell speichern und aus diesem Skript loeschen
model.save(r'/fzi/ids/cl691/no_backup/Training_output_Klassifikation/Modell_speicher/transfer/resnet50/', overwrite=True)
del model

