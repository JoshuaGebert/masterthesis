import tensorflow as tf
from tensorboard.plugins.hparams import api as hp
import random
import numpy as np
import os
import cv2
import matplotlib as plt
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D, Input, GlobalAveragePooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import pickle
import pathlib
from pathlib import Path


datadir = r"/fzi/ids/cl691/trainingsdatensaetze/ova/Wuerfel_Grundkoerper/binary_gut"
datadir_val = r"/fzi/ids/cl691/trainingsdatensaetze/ova/testdata/binary_gut"
AUTOTUNE = tf.data.experimental.AUTOTUNE
data_dir = pathlib.Path(datadir)
data_dir_val = pathlib.Path(datadir_val)

list_ds = tf.data.Dataset.list_files([str(data_dir/'*/*')],  shuffle=True)
list_ds_val = tf.data.Dataset.list_files([str(data_dir_val/'*/*')],  shuffle=True)



def get_label(file_path):

  parts = tf.strings.split(file_path, os.path.sep)


  def no_layershift(): return tf.constant(0)
  def layershift(): return tf.constant(1)
  def no_spaghetti(): return tf.constant(0)
  def spaghetti(): return tf.constant(1)
  def no_warping(): return tf.constant(0)
  def warping(): return tf.constant(1)
  def no_gut(): return tf.constant(0)
  def gut(): return tf.constant(1)
  def no_clogged_nozzle(): return tf.constant(0)
  def clogged_nozzle(): return tf.constant(1)


  label = tf.case([(parts[-2] == tf.constant('no_layershift'), no_layershift),
                   (parts[-2] == tf.constant('layershift'), layershift),
                   (parts[-2] == tf.constant('no_spaghetti'), no_spaghetti),
                   (parts[-2] == tf.constant('spaghetti'), spaghetti) ,
                   (parts[-2] == tf.constant('no_warping'), no_warping),
                   (parts[-2] == tf.constant('warping'), warping),
                   (parts[-2] == tf.constant('no_gut'), no_gut),
                   (parts[-2] == tf.constant('gut'), gut),
                   (parts[-2] == tf.constant('no_clogged_nozzle'), no_clogged_nozzle),
                   (parts[-2] == tf.constant('clogged_nozzle'), clogged_nozzle)
                   ])

  return label


def load_images(file_path):
    # Label einlesen
    label = get_label(file_path)

    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])

    return image_camera, label

def augmentation(file_path):
    # Label einlesen
    label = get_label(file_path)
    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])
    image_camera = tf.image.random_flip_left_right(image_camera)
    image_camera = tf.image.random_brightness(image_camera, max_delta=0.2)

    return image_camera, label



labeled_ds = list_ds.map(load_images, num_parallel_calls=AUTOTUNE).cache()
augmentet_ds = list_ds.map(augmentation, num_parallel_calls=AUTOTUNE)
complete_ds = labeled_ds.concatenate(augmentet_ds)
#complete_ds = labeled_ds
# .shuffle(tf.data.experimental.cardinality(labeled_ds).numpy(), reshuffle_each_iteration=True)

'''
# Dataset in Trainigs-, Validierungs-, Testdaten teilen
DATASET_SIZE = tf.data.experimental.cardinality(complete_ds).numpy()
train_size = int(0.8 * DATASET_SIZE)
val_size = int(0.2 * DATASET_SIZE)

train_ds = complete_ds.take(train_size)
val_ds = complete_ds.skip(train_size)
'''
train_ds = complete_ds
val_ds = list_ds_val.map(load_images, num_parallel_calls=AUTOTUNE)

# Datasets in Batches aufteilen
BATCH_SIZE = 32
train_ds = train_ds.batch(BATCH_SIZE)
val_ds = val_ds.batch (32)

img_heigth = 140
img_width = 310

HP_NUM_UNITS = hp.HParam('num_units', hp.Discrete([64, 128]))
HP_DROPOUT = hp.HParam('dropout', hp.Discrete([0.2, 0.3]))
HP_DROPOUT_1 = hp.HParam('dropout_1', hp.Discrete([0.4, 0.5]))
HP_OPTIMIZER = hp.HParam('optimizer', hp.Discrete(['adam', 'sgd']))
HP_ACTIVATION = hp.HParam('activation', hp.Discrete(['elu', 'relu']))
HP_FILTER = hp.HParam('filters', hp.Discrete([64, 128]))

METRIC_ACCURACY = 'accuracy'

with tf.summary.create_file_writer('/fzi/ids/cl691/Training_output_Klassifikation/Hyperparameter/hparam_tuning/gut').as_default():
  hp.hparams_config(
    hparams=[HP_NUM_UNITS, HP_OPTIMIZER,HP_DROPOUT, HP_DROPOUT_1, HP_ACTIVATION, HP_FILTER],
    metrics=[hp.Metric(METRIC_ACCURACY, display_name='Accuracy')],
  )



def train_test_model(hparams):
    img_heigth = 140
    img_width = 310

    input = Input(shape=(img_heigth, img_width, 1))

    x = Conv2D(hparams[HP_FILTER], (3, 3), activation=hparams[HP_ACTIVATION], input_shape=(140, 310, 1))(input)
    x = Conv2D(hparams[HP_FILTER], (3, 3), activation=hparams[HP_ACTIVATION])(x)
    x = Conv2D(hparams[HP_FILTER], (3, 3), activation=hparams[HP_ACTIVATION])(x)
    x = MaxPooling2D((2, 2))(x)
    x = Dropout(hparams[HP_DROPOUT])(x)
    x = Conv2D(hparams[HP_FILTER], (3, 3), activation=hparams[HP_ACTIVATION])(x)
    x = Conv2D(hparams[HP_FILTER], (3, 3), activation=hparams[HP_ACTIVATION])(x)
    x = Conv2D(hparams[HP_FILTER], (3, 3), activation=hparams[HP_ACTIVATION])(x)
    x = MaxPooling2D((2, 2))(x)
    x = Dropout(hparams[HP_DROPOUT])(x)

    x = GlobalAveragePooling2D()(x)

    x = Dense(hparams[HP_NUM_UNITS], activation=hparams[HP_ACTIVATION])(x)
    x = Dropout(hparams[HP_DROPOUT_1])(x)
    x = Dense(hparams[HP_NUM_UNITS], activation=hparams[HP_ACTIVATION])(x)
    x = Dropout(hparams[HP_DROPOUT_1])(x)
    x = Dense(hparams[HP_NUM_UNITS], activation=hparams[HP_ACTIVATION])(x)

    output = Dense(1, activation='sigmoid')(x)

    model = tf.keras.Model(inputs=input, outputs=output)



    model.compile(
          optimizer=hparams[HP_OPTIMIZER],
          loss='binary_crossentropy',
          metrics=['accuracy'],
    )

    model.fit(train_ds, epochs=30) # Run with 1 epoch to speed things up for demo purposes
    _, accuracy = model.evaluate(val_ds)

    return accuracy

def run(run_dir, hparams):
  with tf.summary.create_file_writer(run_dir).as_default():
    hp.hparams(hparams)  # record the values used in this trial
    accuracy = train_test_model(hparams)
    print(accuracy)
    tf.summary.scalar(METRIC_ACCURACY, accuracy, step=1)


session_num = 0

for num_units in HP_NUM_UNITS.domain.values:
    for dropout in HP_DROPOUT.domain.values:
        for dropout_1 in HP_DROPOUT_1.domain.values:
            for optimizer in HP_OPTIMIZER.domain.values:
                for activation in HP_ACTIVATION.domain.values:
                    for filters in HP_FILTER.domain.values:

                      hparams = {
                          HP_NUM_UNITS: num_units,
                          HP_DROPOUT: dropout,
                          HP_DROPOUT_1: dropout_1,
                          HP_FILTER: filters,
                          HP_ACTIVATION: activation,
                          HP_OPTIMIZER: optimizer,
                      }
                      run_name = "run-%d" % session_num
                      print('--- Starting trial: %s' % run_name)
                      print({h.name: hparams[h] for h in hparams})
                      run('/fzi/ids/cl691/Training_output_Klassifikation/Hyperparameter/hparam_tuning/gut/' + run_name, hparams)
                      session_num += 1