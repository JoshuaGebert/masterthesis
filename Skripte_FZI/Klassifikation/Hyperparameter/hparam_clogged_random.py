import tensorflow as tf
from tensorboard.plugins.hparams import api as hp
import random
import numpy as np
import os
import cv2
import matplotlib as plt
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D, Input, GlobalAveragePooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import pickle
import pathlib
from pathlib import Path


datadir = r"/fzi/ids/cl691/trainingsdatensaetze/ova/Wuerfel_Grundkoerper/binary_clogged_nozzle"
datadir_val = r"/fzi/ids/cl691/trainingsdatensaetze/ova/testdata/binary_clogged"
AUTOTUNE = tf.data.experimental.AUTOTUNE
data_dir = pathlib.Path(datadir)
data_dir_val = pathlib.Path(datadir_val)

list_ds = tf.data.Dataset.list_files([str(data_dir/'*/*')],  shuffle=True)
list_ds_val = tf.data.Dataset.list_files([str(data_dir_val/'*/*')],  shuffle=True)



def get_label(file_path):

  parts = tf.strings.split(file_path, os.path.sep)


  def no_layershift(): return tf.constant(0)
  def layershift(): return tf.constant(1)
  def no_spaghetti(): return tf.constant(0)
  def spaghetti(): return tf.constant(1)
  def no_warping(): return tf.constant(0)
  def warping(): return tf.constant(1)
  def no_gut(): return tf.constant(0)
  def gut(): return tf.constant(1)
  def no_clogged_nozzle(): return tf.constant(0)
  def clogged_nozzle(): return tf.constant(1)


  label = tf.case([(parts[-2] == tf.constant('no_layershift'), no_layershift),
                   (parts[-2] == tf.constant('layershift'), layershift),
                   (parts[-2] == tf.constant('no_spaghetti'), no_spaghetti),
                   (parts[-2] == tf.constant('spaghetti'), spaghetti) ,
                   (parts[-2] == tf.constant('no_warping'), no_warping),
                   (parts[-2] == tf.constant('warping'), warping),
                   (parts[-2] == tf.constant('no_gut'), no_gut),
                   (parts[-2] == tf.constant('gut'), gut),
                   (parts[-2] == tf.constant('no_clogged_nozzle'), no_clogged_nozzle),
                   (parts[-2] == tf.constant('clogged_nozzle'), clogged_nozzle)
                   ])

  return label


def load_images(file_path):
    # Label einlesen
    label = get_label(file_path)

    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])

    return image_camera, label

def augmentation(file_path):
    # Label einlesen
    label = get_label(file_path)
    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])
    image_camera = tf.image.random_flip_left_right(image_camera)
    image_camera = tf.image.random_brightness(image_camera, max_delta=0.2)

    return image_camera, label



labeled_ds = list_ds.map(load_images, num_parallel_calls=AUTOTUNE).cache()
augmentet_ds = list_ds.map(augmentation, num_parallel_calls=AUTOTUNE)
complete_ds = labeled_ds.concatenate(augmentet_ds)
#complete_ds = labeled_ds
# .shuffle(tf.data.experimental.cardinality(labeled_ds).numpy(), reshuffle_each_iteration=True)

'''
# Dataset in Trainigs-, Validierungs-, Testdaten teilen
DATASET_SIZE = tf.data.experimental.cardinality(complete_ds).numpy()
train_size = int(0.8 * DATASET_SIZE)
val_size = int(0.2 * DATASET_SIZE)

train_ds = complete_ds.take(train_size)
val_ds = complete_ds.skip(train_size)
'''
train_ds = complete_ds
val_ds = list_ds_val.map(load_images, num_parallel_calls=AUTOTUNE)

# Datasets in Batches aufteilen
#BATCH_SIZE = 32
#train_ds = train_ds.batch(BATCH_SIZE)
#val_ds = val_ds.batch (32)

img_heigth = 140
img_width = 310

HP_NUM_UNITS = hp.HParam('num_units', hp.Discrete([64, 128, 256]))
HP_DROPOUT = hp.HParam('dropout', hp.Discrete([0.2, 0.3, 0.4]))
HP_DROPOUT_1 = hp.HParam('dropout_1', hp.Discrete([0.3, 0.4, 0.5]))
HP_BATCHSIZE = hp.HParam('batchsize', hp.Discrete([32]))
HP_LEARNINGRATE = hp.HParam('learning', hp.Discrete([0.001, 0.0001]))
HP_ACTIVATION = hp.HParam('activation', hp.Discrete(['elu', 'relu']))
HP_FILTER_1 = hp.HParam('filter_1', hp.Discrete([32, 64, 128]))
HP_FILTER_2 = hp.HParam('filter_2', hp.Discrete([32, 64, 128]))
HP_FILTER_3 = hp.HParam('filter_3', hp.Discrete([32, 64, 128]))
HP_FILTER_4 = hp.HParam('filter_4', hp.Discrete([32, 64, 128]))
HP_KERNELSIZE = hp.HParam('kernelsize', hp.Discrete([3, 4, 5]))

METRIC_ACCURACY = 'accuracy'

with tf.summary.create_file_writer('/fzi/ids/cl691/Training_output_Klassifikation/Hyperparameter/random_tuning_save/clogged').as_default():
  hp.hparams_config(
    hparams=[HP_NUM_UNITS, HP_BATCHSIZE,HP_DROPOUT, HP_DROPOUT_1, HP_ACTIVATION, HP_FILTER_1, HP_FILTER_2,
             HP_FILTER_3,HP_FILTER_4,HP_KERNELSIZE,HP_LEARNINGRATE],
    metrics=[hp.Metric(METRIC_ACCURACY, display_name='Accuracy')],
  )



def train_test_model(hparams, run_name):
    BATCH_SIZE = hparams[HP_BATCHSIZE]

    img_heigth = 140
    img_width = 310

    input = Input(shape=(img_heigth, img_width, 1))

    x = Conv2D(hparams[HP_FILTER_1], (hparams[HP_KERNELSIZE],hparams[HP_KERNELSIZE]), activation=hparams[HP_ACTIVATION], input_shape=(140, 310, 1))(input)
    x = MaxPooling2D((2, 2))(x)
    x = Dropout(hparams[HP_DROPOUT])(x)
    x = Conv2D(hparams[HP_FILTER_2], (hparams[HP_KERNELSIZE],hparams[HP_KERNELSIZE]), activation=hparams[HP_ACTIVATION])(x)
    x = MaxPooling2D((2, 2))(x)
    x = Dropout(hparams[HP_DROPOUT])(x)
    x = Conv2D(hparams[HP_FILTER_3], (hparams[HP_KERNELSIZE],hparams[HP_KERNELSIZE]), activation=hparams[HP_ACTIVATION])(x)
    x = MaxPooling2D((2, 2))(x)
    x = Dropout(hparams[HP_DROPOUT])(x)
    x = Conv2D(hparams[HP_FILTER_4], (hparams[HP_KERNELSIZE],hparams[HP_KERNELSIZE]), activation=hparams[HP_ACTIVATION])(x)
    x = MaxPooling2D((2, 2))(x)
    x = Dropout(hparams[HP_DROPOUT])(x)

    x = GlobalAveragePooling2D()(x)

    x = Dense(hparams[HP_NUM_UNITS], activation=hparams[HP_ACTIVATION])(x)

    output = Dense(1, activation='sigmoid')(x)

    model = tf.keras.Model(inputs=input, outputs=output)

    model.compile(
          optimizer=tf.keras.optimizers.Adam(learning_rate=hparams[HP_LEARNINGRATE]),
          loss='binary_crossentropy',
          metrics=['accuracy'],
    )

    model.fit(train_ds.batch(BATCH_SIZE), epochs=30)  # Run with 1 epoch to speed things up for demo purposes

    model.save('/fzi/ids/cl691/Training_output_Klassifikation/Modell_speicher/hyperparam/clogged/' + run_name)

    _, accuracy = model.evaluate(val_ds.batch(32))

    return accuracy

def run(run_dir, hparams, run_name):
  with tf.summary.create_file_writer(run_dir).as_default():
    hp.hparams(hparams)  # record the values used in this trial
    accuracy = train_test_model(hparams, run_name)
    print(accuracy)
    tf.summary.scalar(METRIC_ACCURACY, accuracy, step=1)


session_num = 10000


for i in range(25):

    num_units = random.choice(HP_NUM_UNITS.domain.values)
    dropout = random.choice(HP_DROPOUT.domain.values)
    dropout_1 = random.choice(HP_DROPOUT_1.domain.values)
    filter_1 = random.choice(HP_FILTER_1.domain.values)
    filter_2 = random.choice(HP_FILTER_2.domain.values)
    filter_3 = random.choice(HP_FILTER_3.domain.values)
    filter_4 = random.choice(HP_FILTER_4.domain.values)
    kernelsize = random.choice(HP_KERNELSIZE.domain.values)
    activation = random.choice(HP_ACTIVATION.domain.values)
    batchsize = random.choice(HP_BATCHSIZE.domain.values)
    learningrate = random.choice(HP_LEARNINGRATE.domain.values)


    hparams = {
        HP_NUM_UNITS: num_units,
        HP_DROPOUT: dropout,
        HP_DROPOUT_1: dropout_1,
        HP_FILTER_1: filter_1,
        HP_FILTER_2: filter_2,
        HP_FILTER_3: filter_3,
        HP_FILTER_4: filter_4,
        HP_KERNELSIZE: kernelsize,
        HP_ACTIVATION: activation,
        HP_BATCHSIZE: batchsize,
        HP_LEARNINGRATE: learningrate
    }
    run_name = "run-%d" % session_num
    print('--- Starting trial: %s' % run_name)
    print({h.name: hparams[h] for h in hparams})
    run('/fzi/ids/cl691/Training_output_Klassifikation/Hyperparameter/random_tuning_save/clogged/' + run_name, hparams, run_name)
    session_num += 1
