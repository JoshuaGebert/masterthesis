import tensorflow as tf
from tensorboard.plugins.hparams import api as hp
import random
import numpy as np
import os
import cv2
import matplotlib as plt
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D, Input, GlobalAveragePooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import pickle

datadir = r"/fzi/ids/cl691/trainingsdatensaetze/ova/binary_clogged_nozzle"
# datadir = r"F:\Trainingsdaten_Master\predictions\wuerfel_test"
#categories = ["gut", "layershift"]
categories = ["no_clogged_nozzle","clogged_nozzle"]

IMG_height = 140
IMG_width = 310

for category in categories:
    path = os.path.join(datadir, category)
    print(path)
    for img in os.listdir(path):
        img_array = cv2.imread(os.path.join(path,img), cv2.IMREAD_GRAYSCALE)

        break
    break

print(img_array.shape)

training_data = []

def create_training_data():
    for category in categories:
        path = os.path.join(datadir, category)
        class_num =categories.index(category)
        print(path)
        for img in os.listdir(path):
            img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
            new_array = cv2.resize(img_array, (IMG_width, IMG_height))
            training_data.append([new_array,class_num])


create_training_data()

print(len(training_data))

random.shuffle(training_data)

X = []
y = []


for features, label in training_data:
    X.append(features)
    y.append(label)

X = np.array(X).reshape(-1, IMG_height, IMG_width, 1)
y = np.array(y)

X = X/255.0

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

HP_NUM_UNITS = hp.HParam('num_units', hp.Discrete([16, 32]))
#HP_LARNINGRATE = hp.HParam('learning_rate', hp.RealInterval(0.1, 0.2))
HP_OPTIMIZER = hp.HParam('optimizer', hp.Discrete(['adam', 'sgd']))
HP_LEARNINGRATE = hp.HParam('learningrate', hp.Discrete([0.]),'learrat')

METRIC_ACCURACY = 'accuracy'

with tf.summary.create_file_writer('/fzi/ids/cl691/Training_output_Klassifikation/Hyperparameter/hparam_tuning').as_default():
  hp.hparams_config(
    hparams=[HP_NUM_UNITS, HP_OPTIMIZER],
    metrics=[hp.Metric(METRIC_ACCURACY, display_name='Accuracy')],
  )



def train_test_model(hparams):
    img_heigth = 140
    img_width = 310

    input = Input(shape=(img_heigth, img_width, 1))

    x = Conv2D(64, (3, 3), activation='relu', input_shape=(140, 310, 1))(input)
    x = MaxPooling2D((2, 2))(x)
    x = Conv2D(64, (3, 3), activation='relu')(x)
    x = MaxPooling2D((2, 2))(x)
    x = Conv2D(64, (3, 3), activation='relu')(x)
    x = MaxPooling2D((2, 2))(x)
    x = Conv2D(64, (3, 3), activation='relu')(x)
    x = MaxPooling2D((2, 2))(x)

    x = GlobalAveragePooling2D()(x)
    # x = Flatten()(x)
    x = Dense(hparams[HP_NUM_UNITS], activation='relu')(x)

    output = Dense(1, activation='sigmoid')(x)

    model = tf.keras.Model(inputs=input, outputs=output)



    model.compile(
          optimizer=hparams[HP_OPTIMIZER],
          loss='binary_crossentropy',
          metrics=['accuracy'],
    )

    model.fit(X_train, y_train, epochs=10) # Run with 1 epoch to speed things up for demo purposes
    _, accuracy = model.evaluate(X_test, y_test)

    return accuracy

def run(run_dir, hparams):
  with tf.summary.create_file_writer(run_dir).as_default():
    hp.hparams(hparams)  # record the values used in this trial
    accuracy = train_test_model(hparams)
    print(accuracy)
    tf.summary.scalar(METRIC_ACCURACY, accuracy, step=1)


session_num = 0

for num_units in HP_NUM_UNITS.domain.values:

    for optimizer in HP_OPTIMIZER.domain.values:
      hparams = {
          HP_NUM_UNITS: num_units,

          HP_OPTIMIZER: optimizer,
      }
      run_name = "run-%d" % session_num
      print('--- Starting trial: %s' % run_name)
      print({h.name: hparams[h] for h in hparams})
      run('/fzi/ids/cl691/Training_output_Klassifikation/Hyperparameter/hparam_tuning/' + run_name, hparams)
      session_num += 1