import tensorflow as tf
from tensorboard.plugins.hparams import api as hp
import random
import numpy as np
import pandas as pd
import os
import cv2
import matplotlib as plt
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D, Input, GlobalAveragePooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import pickle
import pathlib
from pathlib import Path
from os import listdir

klassifikator = "multiclass_mobilenet_1"


datadir_val = r"/fzi/ids/cl691/trainingsdatensaetze/Wuerfel_Grundkoerper_zeitversatz"
AUTOTUNE = tf.data.experimental.AUTOTUNE

data_dir_val = pathlib.Path(datadir_val)

list_ds_val = tf.data.Dataset.list_files([str(data_dir_val/'*/*')],  shuffle=True)

#direct = r"G:/2020_Daten_MA_Joshua_Gebert/Modelle/hyperparam/" + klassifikator
direct = r"/fzi/ids/cl691/Training_output_Klassifikation/Modell_speicher/hyperparam/" + klassifikator
list_mod = os.listdir(str(direct))


def get_label(file_path):

  parts = tf.strings.split(file_path, os.path.sep)


  def gut(): return tf.constant(0)
  def layershift(): return tf.constant(2)
  def clogged_nozzle(): return tf.constant(4)
  def spaghetti(): return tf.constant(3)
  def warping(): return tf.constant(1)

  label = tf.case([(parts[-2] == tf.constant('gut'), gut),
                   (parts[-2] == tf.constant('layershift'), layershift),
                   (parts[-2] == tf.constant('clogged_nozzle'), clogged_nozzle),
                   (parts[-2] == tf.constant('spaghetti'), spaghetti),
                   (parts[-2] == tf.constant('warping'), warping)
                   ])

  return label


def load_images(file_path):
    # Label einlesen
    label = get_label(file_path)

    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])

    return image_camera, label

val_ds = list_ds_val.map(load_images, num_parallel_calls=AUTOTUNE)

# Datasets in Batches aufteilen
val_ds = val_ds.batch (32)
a = 0
length = len(list_mod)
list_acc = []
list_err = []
list_model = []
for i in list_mod:
    print(i)
    model = tf.keras.models.load_model(direct + r"/" + str(i))
    error ,accuracy = model.evaluate(val_ds)
    list_acc.append(accuracy)
    list_err.append(error)
    list_model.append(i)

df = pd.DataFrame(data={"Model": list_model, "accuracy": list_acc, "error": list_err})
df.to_csv(r"/fzi/ids/cl691/beurteilung/" + klassifikator+ ".csv", sep=',', index=False)

