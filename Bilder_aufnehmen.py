# https://stackoverflow.com/questions/474528/what-is-the-best-way-to-repeatedly-execute-a-function-every-x-seconds-in-python
# https://www.geeksforgeeks.org/python-opencv-cv2-imwrite-method/

import cv2
import os

directory = r"E:/2020_Daten_MA_Joshua_Gebert/Trainingsdaten/" # Pfad für Speicherort
eingabe = input("Speicherort:")  # Ordner des Bilder abfgragen
os.chdir(directory)
os.mkdir(directory + eingabe) # Ordner wird automatisch erstellt
os.chdir(directory + eingabe)
print("Dateien sind hier zu finden: " + directory + eingabe)
i = 1
sekunden = int(input("Dauer des Drucks in Sekunden:"))  # Dauer in Sekunden einlesen (Dauer des Drucks)
cam = cv2.VideoCapture(1, cv2.CAP_DSHOW)  # Kamera anschalten;  (0)für Laptop Webcam; (1) für USB Cam
cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)  # Intel RealSenase kann 1920 x 1080
cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

print("press strg + c to stopp")

while i <= sekunden:  # jede Sekunde ausführen, solange Druck dauert
    cv2.waitKey(1000)  # eine Sekunde verzögern

    ret, image = cam.read()  # Bild aufnehmen

    filename = str(i) + ".png"  # Dateiname
    cv2.imwrite(filename, image)  # Befehl zum speichern

    i = i + 1

cam.release()
cv2.destroyAllWindows()
