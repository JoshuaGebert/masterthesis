import cv2
import os

directory = r"C:/Users/User/Documents/robot_modifikation/"   # Pfad für Speicherort abfragen
os.chdir(directory)
print(directory)
i_1 = "ausgangssituation-301mm"
i_2 = "nachvorne-295mm"
i_3 = "nachvorne+290mm"
i_4 = "nachvorne+285mm"
i_5 = "nachhinten-305mm"
i_6 = "nachhinten-310mm"
i_7 = "nachhinten-315mm"
i_8 = "nachhinten-320mm"
i_9 = "nachvorne-280mm"
i_10 = "nachoben-310mm"
i_11 = "nachoben-315mm"
i_12 = "nachunten-320mm"
i_13 = "nachunten-280mm"


cam = cv2.VideoCapture(2, cv2.CAP_DSHOW)  # Kamera anschalten;  (0)für Laptop Webcam; (2) für USB Cam
cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)  # Intel RealSenase kann 1920 x 1080
cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

print("press strg + c to stopp")

ret, image = cam.read()  # Bild aufnehmen

filename = str(i_9) + ".png"  # Dateiname
cv2.imwrite(filename, image)  # Befehl zum speichern

cam.release()
cv2.destroyAllWindows()