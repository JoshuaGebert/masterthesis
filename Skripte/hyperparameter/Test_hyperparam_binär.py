import tensorflow as tf
from tensorboard.plugins.hparams import api as hp
import random
import numpy as np
import os
import cv2
import matplotlib as plt
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D, Input, GlobalAveragePooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import pickle
import pathlib
from pathlib import Path



datadir_val = r"F:\Evaluierungsdaten_Master\binary\Wuerfel_Grundkoerper\binary_layershift"
AUTOTUNE = tf.data.experimental.AUTOTUNE

data_dir_val = pathlib.Path(datadir_val)

list_ds_val = tf.data.Dataset.list_files([str(data_dir_val/'*/*')],  shuffle=True)



def get_label(file_path):

  parts = tf.strings.split(file_path, os.path.sep)


  def no_layershift(): return tf.constant(0)
  def layershift(): return tf.constant(1)
  def no_spaghetti(): return tf.constant(0)
  def spaghetti(): return tf.constant(1)
  def no_warping(): return tf.constant(0)
  def warping(): return tf.constant(1)
  def no_gut(): return tf.constant(0)
  def gut(): return tf.constant(1)
  def no_clogged_nozzle(): return tf.constant(0)
  def clogged_nozzle(): return tf.constant(1)


  label = tf.case([(parts[-2] == tf.constant('no_layershift'), no_layershift),
                   (parts[-2] == tf.constant('layershift'), layershift),
                   (parts[-2] == tf.constant('no_spaghetti'), no_spaghetti),
                   (parts[-2] == tf.constant('spaghetti'), spaghetti) ,
                   (parts[-2] == tf.constant('no_warping'), no_warping),
                   (parts[-2] == tf.constant('warping'), warping),
                   (parts[-2] == tf.constant('no_gut'), no_gut),
                   (parts[-2] == tf.constant('gut'), gut),
                   (parts[-2] == tf.constant('no_clogged_nozzle'), no_clogged_nozzle),
                   (parts[-2] == tf.constant('clogged_nozzle'), clogged_nozzle)
                   ])

  return label


def load_images(file_path):
    # Label einlesen
    label = get_label(file_path)

    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])

    return image_camera, label

val_ds = list_ds_val.map(load_images, num_parallel_calls=AUTOTUNE)

# Datasets in Batches aufteilen
val_ds = val_ds.batch (32)
a = 0
for i in range(10):
    model = tf.keras.models.load_model(r"G:\2020_Daten_MA_Joshua_Gebert\Modelle\hyperparam\layershift\run-" + str(a))
    accuracy = model.evaluate(val_ds)
    print(accuracy)
    a = a + 1