# https://www.tensorflow.org/tensorboard/hyperparameter_tuning_with_hparams

import tensorflow as tf
from tensorboard.plugins.hparams import api as hp
import os
import matplotlib.pyplot as plt
from keras import datasets, layers, models
import pathlib
import numpy as np

def get_label(filename):
    parts = tf.strings.split(filename, os.path.sep)


    def gut(): return tf.constant(0)
    def layershift(): return tf.constant(1)
    def clogged_nozzle(): return tf.constant(1)
    def spaghetti(): return tf.constant(1)
    def warping(): return tf.constant(1)

    label = tf.case([(parts[-2] == tf.constant('gut'), gut),
                     (parts[-2] == tf.constant('layershift'), warping),
                     (parts[-2] == tf.constant('clogged_nozzle'), clogged_nozzle),
                     (parts[-2] == tf.constant('spaghetti'), spaghetti),
                     (parts[-2] == tf.constant('warping'), spaghetti)
                     ] )

    return label

def parse_image(filename):

    label = get_label(filename)
    image = tf.io.read_file(filename)
    image = tf.image.decode_png(image, channels=1)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize(image, [140, 310])

    return  image, label
# define dataset
data_dir = r"E:/Trainingsdaten_Master/Trainingsdatensaetze/datensatz_spaghetti"

data_dir = pathlib.Path(data_dir)

list_ds = tf.data.Dataset.list_files(str(data_dir/'*/*'), shuffle=True)
labeled_ds = list_ds.map(parse_image)

DATASET_SIZE = tf.data.experimental.cardinality(labeled_ds).numpy()
train_size = int(0.8 * DATASET_SIZE)
val_size = int(0.2 * DATASET_SIZE)

train_ds = labeled_ds.take(train_size)
val_ds = labeled_ds.skip(train_size)

# train
x_train = train_ds[0]
y_train = train_ds[1]
# test
x_test = val_ds[0]
y_test = val_ds[1]


# define Parameter
HP_NUM_UNITS = hp.HParam('num_units', hp.Discrete([16, 32]))
HP_DROPOUT = hp.HParam('dropout', hp.RealInterval(0.1, 0.2))
HP_OPTIMIZER = hp.HParam('optimizer', hp.Discrete(['adam', 'sgd']))

METRIC_ACCURACY = 'accuracy'

with tf.summary.create_file_writer('logs/hparam_tuning').as_default():
  hp.hparams_config(
    hparams=[HP_NUM_UNITS, HP_DROPOUT, HP_OPTIMIZER],
    metrics=[hp.Metric(METRIC_ACCURACY, display_name='Accuracy')],
  )

# log
def run(run_dir, hparams):
  with tf.summary.create_file_writer(run_dir).as_default():
    hp.hparams(hparams)  # record the values used in this trial
    accuracy = train_test_model(hparams)
    tf.summary.scalar(METRIC_ACCURACY, accuracy, step=1)
# create model
def train_test_model(hparams):
  model = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(hparams[HP_NUM_UNITS], activation=tf.nn.relu),
    tf.keras.layers.Dropout(hparams[HP_DROPOUT]),
    tf.keras.layers.Dense(1, activation=tf.nn.softmax),
  ])
  model.compile(
      optimizer=hparams[HP_OPTIMIZER],
      loss='sparse_categorical_crossentropy',
      metrics=['accuracy'],
  )
  model.fit(x_train, y_train, epochs=50)
  accuracy = model.evaluate(x_test, y_test)
  return accuracy


session_num = 0

for num_units in HP_NUM_UNITS.domain.values:
  for dropout_rate in (HP_DROPOUT.domain.min_value, HP_DROPOUT.domain.max_value):
    for optimizer in HP_OPTIMIZER.domain.values:
      hparams = {
          HP_NUM_UNITS: num_units,
          HP_DROPOUT: dropout_rate,
          HP_OPTIMIZER: optimizer,
      }
      run_name = "run-%d" % session_num
      print('--- Starting trial: %s' % run_name)
      print({h.name: hparams[h] for h in hparams})
      run('logs/hparam_tuning/' + run_name, hparams)
      session_num += 1
