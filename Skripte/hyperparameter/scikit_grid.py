# Use scikit-learn to grid search the batch size and epochs
import numpy
import os
import tensorflow as tf
import matplotlib.pyplot as plt
import pathlib
from pathlib import Path
from keras import datasets, layers, models
from sklearn.model_selection import GridSearchCV
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
# Function to create model, required for KerasClassifier



def get_label(filename):
    parts = tf.strings.split(filename, os.path.sep)


    def gut(): return tf.constant(0)
    def layershift(): return tf.constant(1)
    def clogged_nozzle(): return tf.constant(1)
    def spaghetti(): return tf.constant(1)
    def warping(): return tf.constant(1)

    label = tf.case([(parts[-2] == tf.constant('gut'), gut),
                     (parts[-2] == tf.constant('layershift'), warping),
                     (parts[-2] == tf.constant('clogged_nozzle'), clogged_nozzle),
                     (parts[-2] == tf.constant('spaghetti'), spaghetti),
                     (parts[-2] == tf.constant('warping'), spaghetti)
                     ] )

    return label

def parse_image(filename):

    label = get_label(filename)
    image = tf.io.read_file(filename)
    image = tf.image.decode_png(image, channels=1)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize(image, [140, 310])

    return  image, label






def create_model(optimizer='adam'):
    img_heigth = 140
    img_width = 310

    input = tf.keras.Input(shape=(img_heigth, img_width, 1))

    x = layers.Conv2D(32, (3, 3), activation='elu', input_shape=(140, 310, 1))(input)
    x = tf.keras.layers.BatchNormalization()(x)
    x = layers.MaxPooling2D((2, 2))(x)
    x = layers.Conv2D(64, (3, 3), activation='elu')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = layers.MaxPooling2D((2, 2))(x)
    x = layers.Conv2D(64, (3, 3), activation='elu')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = layers.Flatten()(x)
    x = layers.Dense(64, activation='elu')(x)

    output = layers.Dense(1, activation='softmax')(x)

    model = tf.keras.Model(inputs=input, outputs=output)
    model.compile(
        optimizer=optimizer,
        loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
        metrics=['accuracy']
    )

    return model
# fix random seed for reproducibility
seed = 7
numpy.random.seed(seed)
# load dataset

data_dir = r"E:/Trainingsdaten_Master/Trainingsdatensaetze/datensatz_spaghetti"

data_dir = pathlib.Path(data_dir)

# Dataset aus nur einem Bild erstellen
list_ds = tf.data.Dataset.list_files(str(data_dir/'*/*'), shuffle=True)
labeled_ds = list_ds.map(parse_image)

X = labeled_ds[0]
Y = labeled_ds[1]
# create model
model = KerasClassifier(build_fn=create_model, epochs=100, batch_size=10, verbose=0)
# define the grid search parameters
optimizer = ['SGD', 'RMSprop', 'Adagrad', 'Adadelta', 'Adam', 'Adamax', 'Nadam']
param_grid = dict(optimizer=optimizer)
grid = GridSearchCV(estimator=model, param_grid=param_grid, n_jobs=-1, cv=5)
grid_result = grid.fit(X, Y)
# summarize results
print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))