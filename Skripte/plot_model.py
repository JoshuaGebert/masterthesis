import tensorflow as tf
from tensorflow.keras.utils import plot_model

model = tf.keras.models.load_model(
    r'C:\Users\User\Documents\Output\Training_output_Klassifikation\Modell_speicher\test\residual')
plot_model(model, to_file='model.png')
