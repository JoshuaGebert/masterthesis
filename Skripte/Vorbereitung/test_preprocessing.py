import cv2
import numpy as np
import math
import os, os.path
from matplotlib import pyplot as plt
from cv2 import Laplacian, Sobel


img = cv2.imread(r"F:\Vorbereitung_testdatensatz\1078.png")

img = img[490:630, 925:1235]  # aktuell: [490:630, 925:1235]


# Graustufen
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# Bild drehen
img = cv2.rotate(img, cv2.ROTATE_180)

hist1 = cv2.calcHist([img],[0],None,[256],[0,256])

plt.plot(hist1)
plt.title('normal')
plt.show()