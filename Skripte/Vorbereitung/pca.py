import tensorflow as tf
import os
import matplotlib.pyplot as plt
from keras import datasets, layers, models
import pathlib
import numpy as np
from datetime import datetime
from tensorflow.keras import datasets, layers, models, activations
from pathlib import Path

directory = r"E:/Trainingsdaten_Master/Trainingsdatensaetze/Datensatz_spaghetti"
directory = pathlib.Path(directory)

train_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
        rescale=1./255,
        rotation_range=5,
        validation_split=0.2
)

train_generator = train_datagen.flow_from_directory(
    directory= directory,
    target_size=(140, 310),
    color_mode="grayscale",
    batch_size=32,
    class_mode="binary",
    shuffle=True,
    subset="training",
    seed=42
)
val_generator = train_datagen.flow_from_directory(
    directory= directory,
    target_size=(140, 310),
    color_mode="grayscale",
    batch_size=32,
    class_mode="binary",
    shuffle=True,
    subset="validation",
    seed=42
)

print(train_generator[0,:])