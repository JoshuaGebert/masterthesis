import numpy as np

import os
import cv2
import pathlib
from pathlib import Path

i = 7693
# Ordner mit Inhalt Struktur der Daten
data_dir = r"C:/Users/User/Documents/Trainingsdaten/Wuerfel"
data_dir = Path(data_dir)  # liest den Pfad ein

# Labels in Array schreiben
label_dir = data_dir / 'labels'
label_names = np.array([item.name for item in label_dir.glob('*')])
for a in label_names:
    print(a)

for xy in label_names:
    p = Path("C:/Users/User/Documents/Trainingsdaten/Wuerfel/labels/" + xy)
    print(p)

    for item in p.glob('**/*/*'
                       ''):
        i = i + 1
        print(str(item))

        if item.suffix in ['.png','.xlsx']:

            img = cv2.imread(str(item))
            # Bild schneiden
            img = img[450:580, 915:1225]  # aktuell: [490:630, 925:1235]

            # Graustufen
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            # Bild drehen
            img = cv2.rotate(img, cv2.ROTATE_180)

            # minimalen/maximalen Grauwert ermitteln
            minimum = 127
            maximum = 127

            for x in range(0, img.shape[1]):
                for y in range(0, img.shape[0]):
                    if img[y][x] < minimum:
                        minimum = img[y][x]
                    elif img[y][x] > maximum:
                        maximum = img[y][x]

            # Grauwerte anpassen
            schwarz = 0
            weiß = 255

            faktor = (weiß - schwarz) / (maximum - minimum)

            for x in range(0, img.shape[1]):
                for y in range(0, img.shape[0]):
                    img[y][x] = (img[y][x] - minimum) * faktor

            # Bilder speichern
            os.chdir(r"E:/Trainingsdaten_Master/Trainingsdaten/Ueberarbeitet/Wuerfel/" + xy )
            cv2.imwrite("Grauw_angegl_" + xy + str(i) + ".png", img)


