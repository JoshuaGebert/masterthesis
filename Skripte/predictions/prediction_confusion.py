
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
import keras
import tensorflow as tf
import time
from sklearn import metrics


def normalize(img):
    '''
    Normalizes an array
    (subtract mean and divide by standard deviation)
    '''
    eps = 0.001
    if np.std(img) != 0:
        img = (img - np.mean(img)) / np.std(img)
    else:
        img = (img - np.mean(img)) / eps
    return img


st = time.time()
stream = "infillfehlerrest-140619.wav"
size = 0.2
directory = ""
length = librosa.core.get_duration(filename=os.path.join(directory, stream))
start = 0  # at which time step to start the prediction
model = keras.models.load_model("lstm_keras_6c_20-02-13-20-21.h5")  # change this to determine the model

# Arrays of the predicted and ground truth labels. pred[0] is e.g. at time = 0.0, pred[0] and label[0] at time = time + overlap
pred = []
labels = []

for i in range(0, round(int(length) / size) * 2 + 1):
    finish = start + size
    # extract features from prediction samples and normalize them
    newAudio, sr = librosa.load(os.path.join(directory, stream), sr=44100, offset=start, duration=finish - start)
    ft = normalize(extract(newAudio, sr))
    k = model.predict_classes(np.array([ft, ]))  # make prediction
    df = pd.read_csv(stream.split('.')[0] + '.txt', sep='	', header=None)  # read ground truth labels
    # Determine Label
    l = -1
    for index, row in df.iterrows():  # for every sample(=row) with its respective start and end times
        # Determine interval borders
        start = round(start, 1)
        finish = round(finish, 1)
        row[0] = round(row[0], 1)
        row[1] = round(row[1], 1)
        l = int(row[2])
        if l == 1 or l == 0:
            l = l
        elif l == 2 or l == 3 or l == 5:
            l = l
        elif l == 4:
            l = l
        if (start >= row[0] and finish <= row[
            1]):  # We only take samples where we are in the interval. Reason: Ambiguity, class would be hard to determine
            labels.append(l)
            pred.append(k)
            if k != l and (l == 5 or l == 0):
                print("False prediction, start: {};Predicted {} should be {}".format(start, k[0], l))
            break
    # Overlap
    start += 0.1

# print results and statistics

print(round(int(length) / size) * 2 + 1)
end = time.time()
print(end - st)  # print time the procedure took
print(np.hstack(pred))  # hstack to that pred/labels becomes a 1-dim array
print(np.hstack(labels))
matrix = metrics.confusion_matrix(labels, pred)
print(matrix)

print("F1-Score, macro: {}".format(metrics.f1_score(labels, pred, average='macro')))
print("Samplesize:{}".format(len(pred)))
