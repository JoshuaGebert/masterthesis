import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from tensorflow import keras
from sklearn.ensemble import StackingClassifier
import pickle
import random
from datetime import datetime

def gamma(image, gamma):
    img1 = np.zeros(shape=(140, 310), dtype='uint8')
    maximum = 127
    for x in range(0, image.shape[1]):
       for y in range(0, image.shape[0]):
            #print(image[y][x])
            if image[y][x] > maximum:
                           maximum = image[y][x]

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            img1[y][x] = 255*(img[y][x]/maximum)**(1/gamma)

    return img1

IMG_width = 310
IMG_height = 140

i = 1
sekunden = int(input("Dauer des Drucks in Sekunden:"))  # Dauer in Sekunden einlesen (Dauer des Drucks)
cam = cv2.VideoCapture(1, cv2.CAP_DSHOW)  # Kamera anschalten;  (0)für Laptop Webcam; (1) für USB Cam
cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)  # Intel RealSenase kann 1920 x 1080
cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

print("press strg + c to stopp")

model_warping = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_12_augmentation_dropout\warping")
model_layershift = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_21_earlystopping\patience_3\layershift")
model_spaghetti = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_21_earlystopping\patience_5\spaghetti")
model_clogged = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_21_earlystopping\patience_3\clogged")
model_gut = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_22_bigmodul\gut")

ausgabe = []
count_warp = 0
count_layer = 0
count_spagh = 0
count_clog = 0
count_fehler = 0

while i <= sekunden:  # jede Sekunde ausführen, solange Druck dauert
    cv2.waitKey(1000)  # eine Sekunde verzögern

    ret, image = cam.read()  # Bild aufnehmen

    img = image

    # falsches Format
    img = cv2.resize(img, dsize=(1920, 1080))

    # Bild schneiden
    img = img[460:600, 955:1265]  # aktuell: [460:600, 955:1265] , [490:630, 925:1235]

    # Graustufen
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Bild drehen
    img = cv2.rotate(img, cv2.ROTATE_180)

    img = gamma(img, 0.8)

    new_array = cv2.resize(img, (IMG_width, IMG_height))

    X = np.array(new_array).reshape(-1, IMG_height, IMG_width, 1)

    X = X / 255.0

    y_pred_warping_1 = model_warping.predict(X)
    y_pred_layershift_1 = model_layershift.predict(X)
    y_pred_spaghetti_1 = model_spaghetti.predict(X)
    y_pred_clogged_1 = model_clogged.predict(X)
    y_pred_gut_1 = model_gut.predict(X)


    pred = []
    pred.append(y_pred_gut_1)
    pred.append(y_pred_warping_1)
    pred.append(y_pred_layershift_1)
    pred.append(y_pred_spaghetti_1)
    pred.append(y_pred_clogged_1)
    vorh = pred.index(max(pred))

    ausgabe.append(vorh)

    if vorh != 0:
        if vorh == 1:
            print("Achtung Warping")
        if vorh == 2:
            print("Achtung Layershift")
        if vorh == 3:
            print("Achtung Spaghetti")
        if vorh == 4:
            print("Achtung Clogged")
    else:
        print("Alles gut")


    i = i + 1

cam.release()
cv2.destroyAllWindows()








