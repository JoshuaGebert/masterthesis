import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
import cv2
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from tensorflow import keras
from sklearn.ensemble import StackingClassifier
import pickle
from pathlib import Path
import random
from datetime import datetime
import re

def sorted_alphanumeric(data):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(data, key=alphanum_key)
def gamma(image, gamma):
    img1 = np.zeros(shape=(140, 310), dtype='uint8')
    maximum = 127
    for x in range(0, image.shape[1]):
       for y in range(0, image.shape[0]):
            #print(image[y][x])
            if image[y][x] > maximum:
                           maximum = image[y][x]

    for x in range(0, image.shape[1]):
        for y in range(0, image.shape[0]):
            img1[y][x] = 255*(img[y][x]/maximum)**(1/gamma)

    return img1

IMG_width = 310
IMG_height = 140



model_warping = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_29_final_hyperparameter\warping")
model_layershift = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_29_final_hyperparameter\layershift")
model_spaghetti = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_29_final_hyperparameter\spaghetti")
model_clogged = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_29_final_hyperparameter\clogged")
model_gut = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_29_final_hyperparameter\gut")

ausgabe = []
y_pred_gut = []
y_pred_warping = []
y_pred_layershift= []
y_pred_spaghetti = []
y_pred_clogged = []
# Ordner mit Inhalt Struktur der Daten
data_dir = r"F:\Trainingsdaten\Rohdaten\Wuerfel\Evaluierung\Wuerfel_20mm_layershift_15_links"
list_files = os.listdir(data_dir)
sorted_list = sorted_alphanumeric(list_files)

#list_files.sort()

for i in sorted_list:  # jede Sekunde ausführen, solange Druck dauert
    #i = sorted_list[a]

    img = cv2.imread(data_dir +"/" + i)

    # falsches Format
    img = cv2.resize(img, dsize=(1920, 1080))

    # Bild schneiden
    img = img[460:600, 955:1265]  # aktuell: [460:600, 955:1265] , [490:630, 925:1235]

    # Graustufen
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Bild drehen
    img = cv2.rotate(img, cv2.ROTATE_180)

    img = gamma(img, 0.8)

    new_array = cv2.resize(img, (IMG_width, IMG_height))

    X = np.array(new_array).reshape(-1, IMG_height, IMG_width, 1)

    X = X / 255.0

    y_pred_warping_1 = model_warping.predict(X)
    y_pred_layershift_1 = model_layershift.predict(X)
    y_pred_spaghetti_1 = model_spaghetti.predict(X)
    y_pred_clogged_1 = model_clogged.predict(X)
    y_pred_gut_1 = model_gut.predict(X)


    y_pred_gut.append(y_pred_gut_1)
    y_pred_warping.append(y_pred_warping_1)
    y_pred_layershift.append(y_pred_layershift_1)
    y_pred_spaghetti.append(y_pred_spaghetti_1)
    y_pred_clogged.append(y_pred_clogged_1)


    pred = []
    pred.append(y_pred_gut_1)
    pred.append(y_pred_warping_1)
    pred.append(y_pred_layershift_1)
    pred.append(y_pred_spaghetti_1)
    pred.append(y_pred_clogged_1)
    vorh = pred.index(max(pred))

    ausgabe.append(vorh)
    '''
    if vorh != 0:
        if vorh == 1:
            title="Achtung Warping"
        if vorh == 2:
            title="Achtung Layershift"
        if vorh == 3:
            title="Achtung Spaghetti"
        if vorh == 4:
            title="Achtung Clogged"
    else:
        title="Alles gut"

    titel = title + "bei" + str(i)

    plt.title(titel)
    plt.imshow(img, cmap='gray')
    os.chdir(r"F:\Zeitverläufe\Zylinder_clogged_lozzle_layer_10_001")
    plt.savefig('Verlauf' + str(i) + '.png', transparent=True)
    plt.show()
    '''
    print(i)

df = pd.DataFrame(data={"vorhersage": ausgabe,"gut": y_pred_gut, "warping": y_pred_warping, "layershift": y_pred_layershift,
                        "spaghetti": y_pred_spaghetti , "clogged": y_pred_clogged })
df.to_csv("F:\Zeitverläufe\Wuerfel_20mm_layershift_15_links\prediction.csv", sep=',',index=False)













