import numpy as np
import tensorflow as tf
from tensorflow import keras
# Display
from IPython.display import Image
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import cv2
import pandas as pd

size = (140, 310)





def cam(img_path):
    from keras.applications.vgg16 import VGG16
    import matplotlib.image as mpimg
    from keras import backend as K
    import matplotlib.pyplot as plt

    K.clear_session()

    model = tf.keras.models.load_model(r"F:\Modelle\mehrklassen\base_functional")
    img = mpimg.imread(img_path)
    plt.imshow(img)
    from keras.preprocessing import image
    img = keras.preprocessing.image.load_img(img_path, target_size=size, color_mode='grayscale')
    # `array` is a float32 Numpy array of shape (299, 299, 3)
    array = keras.preprocessing.image.img_to_array(img)
    # We add a dimension to transform our array into a "batch"
    # of size (1, 299, 299, 3)
    x = np.expand_dims(array, axis=0)
    preds = model.predict(x)
    predictions = pd.DataFrame(decode_predictions(preds, top=3)[0], columns=['col1', 'category', 'probability']).iloc[:,
                  1:]
    argmax = np.argmax(preds[0])
    output = model.output[:, argmax]
    last_conv_layer = model.get_layer('conv2d_1')
    grads = K.gradients(output, last_conv_layer.output)[0]
    pooled_grads = K.mean(grads, axis=(0, 1, 2))
    iterate = K.function([model.input], [pooled_grads, last_conv_layer.output[0]])
    pooled_grads_value, conv_layer_output_value = iterate([x])
    for i in range(512):
        conv_layer_output_value[:, :, i] *= pooled_grads_value[i]
    heatmap = np.mean(conv_layer_output_value, axis=-1)
    heatmap = np.maximum(heatmap, 0)
    heatmap /= np.max(heatmap)
    import cv2
    img = cv2.imread(img_path)
    heatmap = cv2.resize(heatmap, (img.shape[1], img.shape[0]))
    heatmap = np.uint8(255 * heatmap)
    heatmap = cv2.applyColorMap(heatmap, cv2.COLORMAP_JET)
    hif = .8
    superimposed_img = heatmap * hif + img
    output = 'G:/output.jpeg'
    cv2.imwrite(output, superimposed_img)
    img = mpimg.imread(output)
    plt.imshow(img)
    plt.axis('off')
    plt.title(predictions.loc[0, 'category'].upper())
    return None