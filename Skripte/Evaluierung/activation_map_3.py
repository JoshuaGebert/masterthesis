import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.inception_v3 import preprocess_input, decode_predictions
import numpy as np
import os
import keras
import matplotlib.pyplot as plt
import cv2
#from google.colab.patches import cv2_imshow # cv2.imshow does not work on Google Colab notebooks, which is why we are using cv2_imshow instead


img_size = (140, 310)
#preprocess_input = keras.applications.xception.preprocess_input
#decode_predictions = keras.applications.xception.decode_predictions

#model.summary()





# The local path to our target image
img_path = r"F:\Trainingsdaten_Master\Trainingsdatensaetze\Datensatz_ova_multiclass\layershift\Bild_layershift_Wuerfel_layershift_hinten_13_162.png"



def get_img_array(img_path, size):
    # `img` is a PIL image of size 299x299
    img = keras.preprocessing.image.load_img(img_path, target_size=size, color_mode= 'grayscale')
    # `array` is a float32 Numpy array of shape (299, 299, 3)
    array = keras.preprocessing.image.img_to_array(img)

    # We add a dimension to transform our array into a "batch"
    # of size (1, 299, 299, 3)
    array = np.expand_dims(array, axis=0)
    return array

array_1 = get_img_array(img_path,img_size)

print(array_1.shape)


def gradCAM(orig, intensity=0.5, res=250):
    x = get_img_array(orig,(140,310))
    x = x/255

    preds = model.predict(x)
    print(preds)  # prints the class of image

    with tf.GradientTape() as tape:
        last_conv_layer = model.get_layer('conv2d_1')
        iterate = tf.keras.models.Model([model.inputs], [model.output, last_conv_layer.output])
        model_out, last_conv_layer = iterate(x)
        class_out = model_out[:, np.argmax(model_out[0])]
        grads = tape.gradient(class_out, last_conv_layer)
        pooled_grads = K.mean(grads, axis=(0, 1, 2))

    heatmap = tf.reduce_mean(tf.multiply(pooled_grads, last_conv_layer), axis=-1)
    heatmap = np.maximum(heatmap, 0)
    heatmap /= np.max(heatmap)
    #heatmap = heatmap.reshape((8, 8))

    img = cv2.imread(orig)

    heatmap = cv2.resize(heatmap, (img.shape[1], img.shape[0]))

    heatmap = cv2.applyColorMap(np.uint8(255 * heatmap), cv2.COLORMAP_JET)

    img = heatmap * intensity + img

    cv2.imshow(cv2.resize(cv2.imread(orig), (res, res)))
    cv2.imshow(cv2.resize(img, (res, res)))


model = tf.keras.models.load_model(r"F:\Modelle\mehrklassen\base_functional")
model.summary()

img_array = get_img_array(img_path,(140,310))
img_array = img_array/255
# Print what the top predicted class is
preds = model.predict(img_array)
print("predicted",tf.math.argmax(preds, axis=-1, output_type=tf.int32))

gradCAM(img_path)