import tensorflow as tf
import os
import matplotlib.pyplot as plt
from keras import datasets, layers, models
import pathlib
import numpy as np
from datetime import datetime
from tensorflow.keras import datasets, layers, models, activations
from pathlib import Path

directory = r"E:\Trainingsdaten_Master\Trainingsdaten\Trainingsdatensaetze\Datensatz_Klassifikation_balanced\train\labels"
directory = pathlib.Path(directory)

train_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
        rescale=1./255,
        rotation_range=5,
        validation_split=0.2
)

train_generator = train_datagen.flow_from_directory(
    directory= directory,
    target_size=(140, 310),
    color_mode="grayscale",
    batch_size=32,
    class_mode="binary",
    shuffle=True,
    subset="training",
    seed=42
)
val_generator = train_datagen.flow_from_directory(
    directory= directory,
    target_size=(140, 310),
    color_mode="grayscale",
    batch_size=32,
    class_mode="binary",
    shuffle=True,
    subset="validation",
    seed=42
)

# input size
img_heigth = 140
img_width = 310

input = tf.keras.Input(shape=(img_heigth, img_width, 1))

x = layers.Conv2D(32, (3, 3), activation='relu', input_shape=(140, 310, 1))(input)
x = tf.keras.layers.BatchNormalization()(x)
x = layers.MaxPooling2D((2,2))(x)
x = layers.Conv2D(64, (3, 3), activation='relu')(x)
x = tf.keras.layers.BatchNormalization()(x)
x = layers.MaxPooling2D((2,2))(x)
x = layers.Conv2D(64, (3, 3), activation='relu')(x)
x = tf.keras.layers.BatchNormalization()(x)
x = layers.Flatten()(x)
x = layers.Dense(64, activation='relu')(x)

output = layers.Dense(4, activation='softmax')(x)

model = tf.keras.Model(inputs=input, outputs=output)
model.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=0.01),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=['accuracy']
)

model.summary()

STEP_SIZE_TRAIN=train_generator.n//train_generator.batch_size
STEP_SIZE_VALID=val_generator.n//val_generator.batch_size

# Model Plot
# tf.keras.utils.plot_model(model, to_file='model_aktuell.png', show_shapes=True)

# Training + Validiierung
model.fit_generator(generator=train_generator, steps_per_epoch=STEP_SIZE_TRAIN,
                    validation_data=val_generator, validation_steps=STEP_SIZE_VALID,
                    epochs=10)
model.evaluate(generator=val_generator,
steps=STEP_SIZE_VALID)