import tensorflow as tf
import os
import matplotlib.pyplot as plt
import pathlib
from datetime import datetime
from tensorflow.keras import datasets, layers, models, activations
from pathlib import Path

direct_name = r'E:/Trainingsdaten_Master/Trainingsdaten/predictions'
direct_name = pathlib.Path(direct_name)

list_ds = tf.data.Dataset.list_files(str(direct_name/'/*/*'))
print(len(list_ds))

# Reads an image from a file, decodes it into a dense tensor, and resizes it
# to a fixed shape.
def get_label(filename):
    parts = tf.strings.split(filename, os.path.sep)


    def gut(): return tf.constant(0)
    def layershift(): return tf.constant(1)
    def clogged_nozzle(): return tf.constant(2)
    def spaghetti(): return tf.constant(3)

    label = tf.case([(parts[-3] == tf.constant('gut'), gut),
                     (parts[-3] == tf.constant('Layershift'), layershift),
                     (parts[-3] == tf.constant('Clogged_Nozzle'), clogged_nozzle),
                     (parts[-3] == tf.constant('Spaghetti'), spaghetti)
                     ])
    return label




def parse_image(filename):

  label = get_label(filename)

  image = tf.io.read_file(filename)
  image = tf.image.decode_png(image, channels=1)
  image = tf.image.convert_image_dtype(image, tf.float32)
  image = tf.image.resize(image, [140, 310])

  return image, label

predict_ds = list_ds.map(parse_image, deterministic=True)

predict_ds = predict_ds.batch(1)

model = tf.keras.models.load_model(r"C:/Users/User/Documents/Output/Training_output_Klassifikation/Modell_speicher/balanced")

pred = model.predict(predict_ds, verbose=2)

length = len(pred)


tf.math.confusion_matrix(
    predict_ds['label'], pred, num_classes=4, dtype=tf.dtypes.int32
)

