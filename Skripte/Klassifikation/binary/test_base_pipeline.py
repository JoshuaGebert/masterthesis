import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from tensorflow import keras
from sklearn.ensemble import StackingClassifier
import pickle
import random
import pathlib
from datetime import datetime

# data_dir = r"/fzi/ids/cl691/trainingsdatensaetze/Wuerfel"
data_dir = r"F:\Trainingsdaten_Master\predictions\wuerfel_layershift"

data_dir = pathlib.Path(data_dir)

list_ds = tf.data.Dataset.list_files([str(data_dir/'*/*')],  shuffle=True)

def get_label(file_path):

  parts = tf.strings.split(file_path, os.path.sep)


  def no_layershift(): return tf.constant(0)
  def layershift(): return tf.constant(1)


  label = tf.case([(parts[-2] == tf.constant('no_layershift'), no_layershift),
                   (parts[-2] == tf.constant('layershift'), layershift)
                   ])

  return label


def load_images(file_path):
    # Label einlesen
    label = get_label(file_path)

    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])

    return image_camera, label

labeled_ds = list_ds.map(load_images)

labeled_ds = labeled_ds.batch(1)

#batch

model = tf.keras.models.load_model(r"F:\Modelle\binary\ova\layershift_functional_8")
print("mm")

model.summary()

# model = keras.models.load_model(r"/fzi/ids/cl691/Training_output_Klassifikation/Modell_speicher/multiclass")

predict_ds = model.predict(labeled_ds)
print(len(predict_ds))
y_pred = []
y_true = []

for image_camera, labels in labeled_ds.take(2900):
    pred = model.predict(image_camera)
    true = tf.dtypes.cast(labels, tf.int32)
    rounded = tf.math.argmax(pred, axis=-1, output_type=tf.int32)

    y_pred.append(rounded)
    y_true.append(true)

cm = confusion_matrix(y_true, y_pred)
print(cm)

model.evaluate(labeled_ds)