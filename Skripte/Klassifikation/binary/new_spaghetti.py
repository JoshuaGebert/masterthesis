import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
import random
import pickle
datadir = r"F:\Trainingsdaten_Master\Trainingsdatensaetze\datensatz_spaghetti_neu"
categories = ["gut", "spaghetti"]
gesamtliste = ["gut", "warping","layershift","spaghetti","clogged_nozzle"]
IMG_height = 140
IMG_width = 310

for category in categories:
    path = os.path.join(datadir, category)
    print(path)
    for img in os.listdir(path):
        img_array = cv2.imread(os.path.join(path,img), cv2.IMREAD_GRAYSCALE)
        plt.imshow(img_array, cmap="gray")
        plt.show()
        break
    break

print(img_array.shape)

training_data = []

def create_training_data():
    for category in categories:
        path = os.path.join(datadir, category)
        class_num = categories.index(category)

        print(path)
        for img in os.listdir(path):
            img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
            new_array = cv2.resize(img_array, (IMG_width, IMG_height))
            training_data.append([new_array,class_num])


create_training_data()

print(len(training_data))

random.shuffle(training_data)

X = []
y = []

for features, label in training_data:
    X.append(features)
    y.append(label)

X = np.array(X).reshape(-1, IMG_height, IMG_width, 1)
y = np.array(y)

for i in range(3):
    plt.title(y[i])
    plt.imshow(X[i])
    plt.show()

pickle_out = open("X_spaghetti.pickle","wb")
pickle.dump(X, pickle_out)
pickle_out.close()

pickle_out = open("y_spaghetti.pickle","wb")
pickle.dump(y, pickle_out)
pickle_out.close()