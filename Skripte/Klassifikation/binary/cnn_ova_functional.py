import tensorflow as tf
import numpy as np
import keras
import matplotlib.pyplot as plt
import os
import cv2
from tensorflow.keras.models import Sequential
import pathlib
from pathlib import Path
AUTOTUNE = tf.data.experimental.AUTOTUNE
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D, Input, GlobalAveragePooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import pickle
import random
from datetime import datetime

datadir = r"F:\Trainingsdaten_Master\Trainingsdatensaetze\Datensatz_ova_layershift_unbalanced"

data_dir = pathlib.Path(datadir)

list_ds = tf.data.Dataset.list_files([str(data_dir/'*/*')],  shuffle=True)

def get_label(file_path):

  parts = tf.strings.split(file_path, os.path.sep)


  def no_spaghetti(): return tf.constant(0)
  def spaghetti(): return tf.constant(1)


  label = tf.case([(parts[-2] == tf.constant('no_layershift'), no_spaghetti),
                   (parts[-2] == tf.constant('layershift'), spaghetti)
                   ])

  return label


def load_images(file_path):
    # Label einlesen
    label = get_label(file_path)

    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])

    return image_camera, label

complete_ds = list_ds.map(load_images, num_parallel_calls=AUTOTUNE).cache()

# .shuffle(tf.data.experimental.cardinality(labeled_ds).numpy(), reshuffle_each_iteration=True)

# Dataset in Trainigs-, Validierungs-, Testdaten teilen
DATASET_SIZE = tf.data.experimental.cardinality(complete_ds).numpy()
train_size = int(0.8 * DATASET_SIZE)
val_size = int(0.2 * DATASET_SIZE)

train_ds = complete_ds.take(train_size)
val_ds = complete_ds.skip(train_size)

# Datasets in Batches aufteilen
BATCH_SIZE = 32
train_ds = train_ds.batch(BATCH_SIZE)
val_ds = val_ds.batch (32)

img_heigth = 140
img_width = 310

input = Input(shape=(img_heigth, img_width, 1))

x = Conv2D(64, (3, 3), activation='relu', padding='same', input_shape=(140, 310, 1))(input)
x = MaxPooling2D((2,2))(x)
x = Conv2D(64, (3, 3), activation='relu', padding='same')(x)
x = MaxPooling2D((2,2))(x)

# x = Flatten()(x)
x = Dense(64)(x)
x = GlobalAveragePooling2D()(x)

output = Dense(1, activation='sigmoid')(x)

model = tf.keras.Model(inputs=input, outputs=output)

model.compile(loss="binary_crossentropy",
              optimizer="adam",
              metrics=["accuracy"])

model.summary()



#log_dir = "F:\Tensorboard\date_" + datetime.now().strftime("%Y%m%d-%H%M%S") \
#          + "_mehrklassen_wuerfel-base"
#tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, update_freq="batch", histogram_freq=1)

model.fit(train_ds,  epochs=8, validation_data=val_ds)

model.save(r"F:\Modelle\binary\ova\layershift_functional_8")




