import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
from tensorflow.keras.models import Sequential

from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from tensorflow import keras
from sklearn.ensemble import StackingClassifier
import pickle
import random
import pathlib
from datetime import datetime

# data_dir = r"/fzi/ids/cl691/trainingsdatensaetze/Wuerfel"
datadir =  r"F:\Evaluierungsdaten_Master\Wuerfel"

data_dir = pathlib.Path(datadir)

list_ds = tf.data.Dataset.list_files([str(data_dir/'*/*')],  shuffle=True)

def get_label(file_path):

  parts = tf.strings.split(file_path, os.path.sep)


  def gut(): return tf.constant(0)
  def layershift(): return tf.constant(2)
  def clogged_nozzle(): return tf.constant(4)
  def spaghetti(): return tf.constant(3)
  def warping(): return tf.constant(1)

  label = tf.case([(parts[-2] == tf.constant('gut'), gut),
                   (parts[-2] == tf.constant('layershift'), layershift),
                   (parts[-2] == tf.constant('clogged_nozzle'), clogged_nozzle),
                   (parts[-2] == tf.constant('spaghetti'), spaghetti),
                   (parts[-2] == tf.constant('warping'), warping)
                   ])

  return label


def load_images(file_path):
    # Label einlesen
    label = get_label(file_path)

    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])

    return image_camera, label

def augmentation(file_path):
    # Label einlesen
    label = get_label(file_path)
    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])
    image_camera = tf.image.random_flip_left_right(image_camera)
    image_camera = tf.image.random_brightness(image_camera, max_delta=0.2)

    return image_camera, label



labeled_ds = list_ds.map(load_images)

labeled_ds = labeled_ds.batch(1)

#batch

model_gut = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_2_globalaverage\gut_augmentation")
print("warping")
model_warping = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_2_globalaverage\warping_augmentation")
model_layershift = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_2_globalaverage\layershift_augmentation")
model_spaghetti = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_2_globalaverage\spaghetti_augmentation")
model_clogged = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_2_globalaverage\clogged_augmentation")

model_mutliclass = tf.keras.models.load_model(r"F:\Modelle\mehrklassen\multiclass_augmentation_10ep")

print("mm")

model_mutliclass.summary()

# model = keras.models.load_model(r"/fzi/ids/cl691/Training_output_Klassifikation/Modell_speicher/multiclass")



for image_camera, labels in labeled_ds.take(100):

    y_pred_multi = np.argmax(model_mutliclass.predict(image_camera), axis=-1)
    y_pred_gut = (model_gut.predict(image_camera) > 0.5).astype("int32")
    y_pred_warping = (model_warping.predict(image_camera) > 0.5).astype("int32")
    y_pred_clogged = (model_clogged.predict(image_camera) > 0.5).astype("int32")
    y_pred_spaghetti = (model_spaghetti.predict(image_camera) > 0.5).astype("int32")
    y_pred_layershift = (model_layershift.predict(image_camera) > 0.5).astype("int32")


    print(labels, y_pred_multi, y_pred_gut, y_pred_warping, y_pred_layershift, y_pred_spaghetti, y_pred_clogged)




