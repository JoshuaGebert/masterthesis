import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from tensorflow import keras
import pickle
import random
from datetime import datetime

datadir = r"F:\Trainingsdaten_Master\Trainingsdatensaetze\Datensatz_sphere"
#categories = ["gut", "layershift"]
categories = ["gut", "warping"]

IMG_height = 140
IMG_width = 310

for category in categories:
    path = os.path.join(datadir, category)
    print(path)
    for img in os.listdir(path):
        img_array = cv2.imread(os.path.join(path,img), cv2.IMREAD_GRAYSCALE)
        plt.imshow(img_array, cmap="gray")
        plt.show()
        break
    break

print(img_array.shape)

training_data = []

def create_training_data():
    for category in categories:
        path = os.path.join(datadir, category)
        class_num =categories.index(category)
        print(path)
        for img in os.listdir(path):
            img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
            new_array = cv2.resize(img_array, (IMG_width, IMG_height))
            training_data.append([new_array,class_num])


create_training_data()

print(len(training_data))

random.shuffle(training_data)

X = []
y = []


for features, label in training_data:
    X.append(features)
    y.append(label)

X = np.array(X).reshape(-1, IMG_height, IMG_width, 1)
y = np.array(y)

X = X/255.0

model = tf.keras.models.load_model(r"F:\Modelle\binary\base\warping")
#model = tf.keras.models.load_model(r"F:\Modelle\mehrklassen\base")
y_pred = model.predict_classes(X)

cm = confusion_matrix(y, y_pred)
print(cm)
model.evaluate(X, y)
fig = plt.figure(figsize=(20,20))

for i in range(10):
    fig.add_subplot(2,5,i+1)
    plt.title(y_pred[i])
    plt.imshow(X[i], cmap='gray')

plt.show()