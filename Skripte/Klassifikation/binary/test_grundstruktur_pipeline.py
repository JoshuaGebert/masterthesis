import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from tensorflow import keras
from sklearn.ensemble import StackingClassifier
import pickle
import random
from datetime import datetime


datadir = r"F:\Evaluierungsdaten_Master\binary\Wuerfel_Grundkoerper\binary_warping"

#categories = ["no_clogged_nozzle", "clogged_nozzle"]
categories = ["no_warping", "warping"]


IMG_height = 140
IMG_width = 310

for category in categories:
    path = os.path.join(datadir, category)
    print(path)
    for img in os.listdir(path):
        img_array = cv2.imread(os.path.join(path,img), cv2.IMREAD_GRAYSCALE)
        plt.imshow(img_array, cmap="gray")
        plt.show()
        break
    break

print(img_array.shape)

training_data = []

def create_training_data():
    for category in categories:
        path = os.path.join(datadir, category)
        class_num =categories.index(category)
        print(path)
        for img in os.listdir(path):
            img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
            new_array = cv2.resize(img_array, (IMG_width, IMG_height))
            training_data.append([new_array,class_num])


create_training_data()

print(len(training_data))

random.shuffle(training_data)

X = []
y = []


for features, label in training_data:
    X.append(features)
    y.append(label)

X = np.array(X).reshape(-1, IMG_height, IMG_width, 1)
y = np.array(y)

X = X/255.0


#model_base = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_20_komb-batchnorm\gut")
#model_augm = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_11_augmentation\gut")
#model_early5 = tf.keras.models.load_model(r"F:\Modelle\binary\ova\r"F:\Modelle\binary\ova\test_21_earlystopping\patience_3\clogged")

model_1 = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_18_dropout\warping")
model_2 = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_12_augmentation_dropout\warping")
model_3 = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_23_samenetstructure\warping")
model_4 = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_24_hyperparam\warping")

'''
model_base_1 = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_15_classactivationmap\gut")
model_augm_1 = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_14_classactivationmap_augmentation\gut")
model_drop_1 = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_17_classactivationmap_dropout\gut")
model_drop_augm_1 = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_16_classactivationmap_augmentation_drop\gut")
'''
#y_pred_ = (model_drop_augm.predict(X) > 0.5).astype("int32")
y_pred_1 = (model_1.predict(X) > 0.5).astype("int32")
y_pred_2 = (model_2.predict(X) > 0.5).astype("int32")
y_pred_3 = (model_3.predict(X) > 0.5).astype("int32")
y_pred_4 = (model_4.predict(X) > 0.5).astype("int32")



'''
y_pred_5 = (model_base_1.predict(X) > 0.5).astype("int32")
y_pred_6 = (model_augm_1.predict(X) > 0.5).astype("int32")
y_pred_7 = (model_drop_1.predict(X) > 0.5).astype("int32")
y_pred_8 = (model_drop_augm_1.predict(X) > 0.5).astype("int32")
'''
cm = confusion_matrix(y, y_pred_1)
print(cm)

cm = confusion_matrix(y, y_pred_2)
print(cm)


cm = confusion_matrix(y, y_pred_3)
print(cm)

cm = confusion_matrix(y, y_pred_4)
print(cm)







