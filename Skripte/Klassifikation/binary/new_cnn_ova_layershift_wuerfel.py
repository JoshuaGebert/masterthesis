import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D, BatchNormalization
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import pickle
import random
from datetime import datetime

pickle_in = open("X_ova_clogged_unbalanced.pickle","rb")
X = pickle.load(pickle_in)

pickle_in = open("y_ova_clogged_unbalanced.pickle","rb")
y = pickle.load(pickle_in)

X = X/255.0


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)


model = Sequential()

model.add(Conv2D(64,(3,3), input_shape = X.shape[1:]))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.4))
model.add(Conv2D(64, (3,3)))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.4))

model.add(Flatten())
model.add(Dense(64))
model.add(Dropout(0.4))

model.add(Dense(1))
model.add(Activation("sigmoid"))

model.compile(loss="binary_crossentropy",
              optimizer="adam",
              metrics=["accuracy"])

model.summary()

log_dir = "F:\Tensorboard\date_" + datetime.now().strftime("%Y%m%d-%H%M%S")\
          + "ova_clogged_wuerfel_unbalanced-base"
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, update_freq="batch", histogram_freq=1)



model.fit(X_train, y_train, batch_size=32, epochs=3, validation_split=0.2, callbacks=[tensorboard_callback])

model.save(r"F:\Modelle\binary\ova\clogged_nozzle_2")

y_pred = model.predict_classes(X_test)

cm = confusion_matrix(y_test, y_pred)
print(cm)

fig1 = plt.figure(figsize=(20,20))

for i in range(10):
    n = random.randint(1, 978)
    fig1.add_subplot(2,5,i+1)
    plt.title(y_pred[n])
    plt.imshow(X_test[n])
    plt.imshow(X_test[n], cmap='gray')

plt.show()

fig2 = plt.figure(figsize=(20,20))

for i in range(10):
    n = random.randint(1, 978)
    fig2.add_subplot(2,5,i+1)
    plt.title(y_pred[n])
    plt.imshow(X_test[n])
    plt.imshow(X_test[n], cmap='gray')

plt.show()







