from tensorflow.keras.applications.resnet50 import ResNet50
from tensorflow.keras.applications.vgg16 import VGG16
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras import Model

from tensorflow.keras.optimizers import Adam
from tensorflow.keras import datasets, layers, models, activations
import numpy as np
import matplotlib.pyplot as plt
import os
import pathlib
from pathlib import Path
from datetime import datetime

AUTOTUNE = tf.data.experimental.AUTOTUNE

# Ordner mit Inhalt Struktur der Daten
data_dir = r'F:/Trainingsdaten_Master/Trainingsdatensaetze/Datensatz_Klassifikation_balanced_small/train'
data_dir = pathlib.Path(data_dir)  # liest den Pfad ein

# Labels in Array schreiben
label_dir = data_dir / 'labels'
label_names = np.array([item.name for item in label_dir.glob('*')])

# Dataset aus allen Dateipfaden anlegen
list_ds = tf.data.Dataset.list_files([str(data_dir / 'labels/*/*')], shuffle=True)


# Label abh. von Kamerabild einlesen.
def get_label(filename):
    parts = tf.strings.split(filename, os.path.sep)


    def gut(): return tf.constant(0)
    def layershift(): return tf.constant(1)
    def clogged_nozzle(): return tf.constant(2)
    def spaghetti(): return tf.constant(3)

    label = tf.case([(parts[-2] == tf.constant('gut'), gut),
                     (parts[-2] == tf.constant('layershift'), layershift),
                     (parts[-2] == tf.constant('clogged_nozzle'), clogged_nozzle),
                     (parts[-2] == tf.constant('spaghetti'), spaghetti)
                     ])
    return label




def parse_image(filename):

  label = get_label(filename)

  image = tf.io.read_file(filename)
  image = tf.image.decode_png(image, channels=3)
  image = tf.image.convert_image_dtype(image, tf.float32)
  image = tf.image.resize(image, [140, 310])

  return image, label


def augmentation(filename):
    label = get_label(filename)

    image = tf.io.read_file(filename)
    image = tf.image.decode_png(image, channels=3)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize(image, [140, 310])
    image_camera = tf.image.random_brightness(image, max_delta=0.2)

    return image, label


labeled_ds = list_ds.map(parse_image, num_parallel_calls=AUTOTUNE).cache()
augmentet_ds = list_ds.map(augmentation, num_parallel_calls=AUTOTUNE)
complete_ds = labeled_ds.concatenate(augmentet_ds)

# Dataset shuffle
# complete_ds = complete_ds.shuffle(tf.data.experimental.cardinality(complete_ds).numpy(), reshuffle_each_iteration=True)

# Dataset in Trainigs-, Validierungs-, Testdaten teilen
DATASET_SIZE = tf.data.experimental.cardinality(complete_ds).numpy()
train_size = int(0.8 * DATASET_SIZE)
val_size = int(0.2 * DATASET_SIZE)

train_ds = complete_ds.take(train_size)
val_ds = complete_ds.skip(train_size)

BATCH_SIZE = 32
train_ds = train_ds.batch(BATCH_SIZE)
val_ds = val_ds.batch(32)


img_heigth = 140
img_width = 310


InputLayer = tf.keras.Input(shape=(img_heigth, img_width, 3), name='camera_image')

############################## VGG16 ##############################

resnet_model = VGG16(include_top=False, input_shape=(140,310,3))

x = resnet_model.layers[-1].output
x = tf.keras.layers.Dropout(0.5)(x)
x = tf.keras.layers.Flatten()(x)
x = tf.keras.layers.Dense(units=256)(x)
output = tf.keras.layers.Dense(units=4, activation='softmax')(x)

model = Model(inputs=resnet_model.input, outputs=output)

for layer in model.layers[:-1]:
    layer.trainable = False


model.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=0.01),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(),
    metrics=['accuracy']
)

# Model Plot
# tf.keras.utils.plot_model(model, to_file='model_aktuell.png', show_shapes=True)
model.summary()

# Training + Validiierung
model.fit(train_ds, epochs=10, validation_data=val_ds)
