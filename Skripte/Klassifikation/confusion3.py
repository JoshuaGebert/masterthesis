import tensorflow as tf
import os
import matplotlib.pyplot as plt
import pathlib
from datetime import datetime
from tensorflow.keras import datasets, layers, models, activations
from pathlib import Path


data_dir = r"E:/Trainingsdaten_Master/Trainingsdaten/predictions"

data_dir = pathlib.Path(data_dir)

# Dataset aus nur einem Bild erstellen
list_ds = tf.data.Dataset.list_files([str(data_dir / '*/*')], shuffle=False)


# Reads an image from a file, decodes it into a dense tensor, and resizes it
# to a fixed shape.
def show(image, label):
  plt.figure()
  plt.imshow(image)
  plt.title(label.numpy().decode('utf-8'))
  plt.axis('off')


def get_label(file_path):
    parts = tf.strings.split(file_path, os.path.sep)

    def gut(): return tf.constant(0)

    def layershift(): return tf.constant(1)

    def clogged_nozzle(): return tf.constant(2)

    def spaghetti(): return tf.constant(3)

    label = tf.case([(parts[-2] == tf.constant('gut'), gut),
                     (parts[-2] == tf.constant('layershift'), layershift),
                     (parts[-2] == tf.constant('clogged_nozzle'), clogged_nozzle),
                     (parts[-2] == tf.constant('spaghetti'), spaghetti)
                     ])

    return label


def parse_image(filename):
    label = get_label(filename)

    image = tf.io.read_file(filename)
    image = tf.image.decode_png(image, channels=1)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize(image, [140, 310])

    return image


predict_ds = list_ds.map(parse_image)
print("test1")

for f in list_ds.take(20):
    parts = tf.strings.split(f, os.path.sep)
    print(parts[-2])

file_path = next(iter(list_ds))
print(file_path)
image= parse_image(file_path)

print( image)

#for image, label in predict_ds.take(2):
#      print("image, label")

print("tst3")
model = tf.keras.models.load_model(
    r'C:\Users\User\Documents\Output\Training_output_Klassifikation\Modell_speicher\test\baseline')
print("tst3")
model.summary()

pred = model.predict(predict_ds, verbose=1)

'''
length = len(pred)
print("test4")

tf.math.confusion_matrix(
    predict_ds, pred, num_classes=4, dtype=tf.dtypes.int32
)
'''