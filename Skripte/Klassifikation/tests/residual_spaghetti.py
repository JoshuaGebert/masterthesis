import tensorflow as tf
import os
import matplotlib.pyplot as plt
from keras import datasets, layers, models
import pathlib
import numpy as np
from datetime import datetime
from tensorflow.keras import datasets, layers, models, activations
from pathlib import Path



AUTOTUNE = tf.data.experimental.AUTOTUNE

data_dir = r"E:/Trainingsdaten_Master/Trainingsdatensaetze/Datensatz_spaghetti"

data_dir = pathlib.Path(data_dir)

# Dataset aus nur einem Bild erstellen

list_ds = tf.data.Dataset.list_files(str(data_dir/'*/*'), shuffle=True)
file_path = next(iter(list_ds))
print(file_path)
# Reads an image from a file, decodes it into a dense tensor, and resizes it
# to a fixed shape.


def show(image, label):
  plt.figure()
  plt.imshow(image)
  plt.axis('off')
  plt.title(label.numpy())
  plt.show()


import scipy.ndimage as ndimage

def random_rotate_image(image):
  image = ndimage.rotate(image, np.random.uniform(-10, 10), reshape=False)
  return image

def tf_random_rotate_image(image, label):
  im_shape = image.shape
  [image,] = tf.py_function(random_rotate_image, [image], [tf.float32])
  image.set_shape(im_shape)
  return image, label

def get_label(filename):
    parts = tf.strings.split(filename, os.path.sep)


    def gut(): return tf.constant(0)
    def layershift(): return tf.constant(1)
    def clogged_nozzle(): return tf.constant(1)
    def spaghetti(): return tf.constant(1)
    def warping(): return tf.constant(1)

    label = tf.case([(parts[-2] == tf.constant('gut'), gut),
                     (parts[-2] == tf.constant('layershift'), warping),
                     (parts[-2] == tf.constant('clogged_nozzle'), clogged_nozzle),
                     (parts[-2] == tf.constant('spaghetti'), spaghetti),
                     (parts[-2] == tf.constant('warping'), spaghetti)
                     ] )

    return label

def parse_image(filename):

    label = get_label(filename)
    image = tf.io.read_file(filename)
    image = tf.image.decode_png(image, channels=1)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize(image, [140, 310])

    return  image, label

@tf.function
def leakyrelu(z, alpha):
    test = max(alpha * z, z)
    test = tf.cast(test, tf.float32)
    return test


@tf.function
def my_leaky_relu(x):
    return tf.nn.leaky_relu(x, alpha=0.01)


# Datensatz erstellen
labeled_ds = list_ds.map(parse_image, num_parallel_calls=AUTOTUNE)
rot_ds = labeled_ds.map(tf_random_rotate_image, num_parallel_calls=AUTOTUNE)
complete_ds = labeled_ds.concatenate(rot_ds)

# Datensatz in Trainigs-, Validierungs-, Testdaten teilen
DATASET_SIZE = tf.data.experimental.cardinality(complete_ds).numpy()
train_size = int(0.8 * DATASET_SIZE)
val_size = int(0.2 * DATASET_SIZE)

train_ds = complete_ds.take(train_size)
val_ds = complete_ds.skip(train_size)

# Datensatz in Batches aufteilen
BATCH_SIZE = 32
train_ds = train_ds.batch(BATCH_SIZE)
val_ds = val_ds.batch(16)



############################## Residual Block ##############################

def residual_block(y, nb_channels, _strides=(1, 1), _project_shortcut=False):
    shortcut = y

    # down-sampling is performed with a stride of 2
    y = layers.Conv2D(nb_channels, kernel_size=(3, 3), strides=_strides, padding='same')(y)
    y = layers.BatchNormalization()(y)
    y = layers.LeakyReLU()(y)

    y = layers.Conv2D(nb_channels, kernel_size=(3, 3), strides=(1, 1), padding='same')(y)
    y = layers.BatchNormalization()(y)

    # identity shortcuts used directly when the input and output are of the same dimensions
    if _project_shortcut or _strides != (1, 1):
        # when the dimensions increase projection shortcut is used to match dimensions (done by 1×1 convolutions)
        # when the shortcuts go across feature maps of two sizes, they are performed with a stride of 2
        shortcut = layers.Conv2D(nb_channels, kernel_size=(1, 1), strides=_strides, padding='same')(shortcut)
        shortcut = layers.BatchNormalization()(shortcut)

    y = layers.add([shortcut, y])
    y = layers.LeakyReLU()(y)

    return y

############################## Netz ##############################
img_heigth = 140
img_width = 310

# Inputlayers
input = layers.Input(shape=(img_heigth, img_width, 1))

x = layers.Conv2D(filters=64, kernel_size=5, strides=(1, 1), padding='same', activation='elu')(input)
x = layers.BatchNormalization()(x)
x = layers.Conv2D(filters=64, kernel_size=5, strides=(1, 1), padding='same', activation='elu')(x)
x = layers.BatchNormalization()(x)
x = layers.MaxPool2D(pool_size=(2, 2))(x)
x = layers.Dropout(0.25)(x)

x = residual_block(x,128,(1,1),True)

x = layers.Conv2D(filters=128, kernel_size=3, strides=(1, 1), padding='same', activation='elu')(x)
x = layers.BatchNormalization()(x)
x = layers.MaxPool2D(pool_size=(2, 2))(x)
x = layers.Dropout(0.25)(x)

x = layers.Conv2D(filters=256, kernel_size=3, strides=(1, 1), padding='same', activation='elu')(x)
x = layers.BatchNormalization()(x)

x = residual_block(x,256,(1,1),True)

x = layers.Conv2D(filters=256, kernel_size=3, strides=(1, 1), padding='same', activation='elu')(x)
x = layers.BatchNormalization()(x)
x = layers.MaxPool2D(pool_size=(2, 2))(x)
x = layers.Dropout(0.25)(x)

x = layers.Flatten()(x)
x = layers.Dense(240, activation='elu')(x)
x = layers.Dense(240, activation='elu')(x)


output = layers.Dense(4, activation='softmax')(x)


# Modell bauen
model = tf.keras.Model(inputs=input, outputs=output)

model.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=0.01),
    loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
    metrics=['accuracy']
)

# Model Plot
# tf.keras.utils.plot_model(model, to_file='model_aktuell.png', show_shapes=True)
model.summary()

# Tensorboard callback
log_dir = r'E:\Training_output_Klassifikation\Tensorboard\Date' + datetime.now().strftime(
    "%Y%m%d-%H%M%S") + 'residual-spaghetti'
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=2)

# Training + Validiierung
model.fit(train_ds, epochs=100, validation_data=val_ds,
          callbacks=[tensorboard_callback])

# komplettes Modell speichern und aus diesem Skript loeschen
model.save('E:/Training_output_Klassifikation/Modell_speicher/aktuell/spaghetti/residual', overwrite=True)

del model

