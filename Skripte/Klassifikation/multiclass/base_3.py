import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import pickle
import random
import pathlib
from datetime import datetime


AUTOTUNE = tf.data.experimental.AUTOTUNE

data_dir = r"F:/Trainingsdaten_Master/Wuerfel"

data_dir = pathlib.Path(data_dir)

list_ds = tf.data.Dataset.list_files([str(data_dir/'*/*')],  shuffle=True)

def get_label(file_path):

  parts = tf.strings.split(file_path, os.path.sep)


  def gut(): return tf.constant(0)
  def layershift(): return tf.constant(2)
  def clogged_nozzle(): return tf.constant(3)
  def spaghetti(): return tf.constant(4)
  def warping(): return tf.constant(1)

  label = tf.case([(parts[-2] == tf.constant('gut'), gut),
                   (parts[-2] == tf.constant('layershift'), layershift),
                   (parts[-2] == tf.constant('clogged_nozzle'), clogged_nozzle),
                   (parts[-2] == tf.constant('spaghetti'), spaghetti),
                   (parts[-2] == tf.constant('warping'), warping)
                   ])

  return label


def load_images(file_path):
    # Label einlesen
    label = get_label(file_path)

    # Kamerabild laden
    image_camera = tf.io.read_file(file_path)
    image_camera = tf.image.decode_png(image_camera, channels=1)
    image_camera = tf.image.convert_image_dtype(image_camera, dtype=tf.float32)
    image_camera = tf.image.resize(image_camera, [140, 310])

    return image_camera, label

complete_ds = list_ds.map(load_images, num_parallel_calls=AUTOTUNE).cache()

# .shuffle(tf.data.experimental.cardinality(labeled_ds).numpy(), reshuffle_each_iteration=True)

# Dataset in Trainigs-, Validierungs-, Testdaten teilen
DATASET_SIZE = tf.data.experimental.cardinality(complete_ds).numpy()
train_size = int(0.8 * DATASET_SIZE)
val_size = int(0.2 * DATASET_SIZE)

train_ds = complete_ds.take(train_size)
val_ds = complete_ds.skip(train_size)

# Datasets in Batches aufteilen
BATCH_SIZE = 32
train_ds = train_ds.batch(BATCH_SIZE)
val_ds = val_ds.batch (32)

img_heigth = 140
img_width = 310



model = Sequential()

model.add(Conv2D(64, (3, 3), input_shape=((img_heigth, img_width, 1))))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3)))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dense(64))

model.add(Dense(5))
model.add(Activation("softmax"))

model.compile(loss="sparse_categorical_crossentropy",
              optimizer="adam",
              metrics=["accuracy"])

model.summary()

'''
log_dir = "F:\Tensorboard\date_" + datetime.now().strftime("%Y%m%d-%H%M%S") \
         + "all_multiclass_wuerfel"
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, update_freq="batch", histogram_freq=1)

model.fit(train_ds, epochs = 1, validation_data= val_ds, callbacks=[tensorboard_callback])

model.save(r"F:\Modelle\mehrklassen\alldata")


model.evaluate(X_test, y_test)
y_pred = model.predict_classes(X_test)

cm = confusion_matrix(y_test, y_pred, )
print(cm)

fig = plt.figure(figsize=(20, 20))

for i in range(10):
    fig.add_subplot(2, 5, i + 1)
    n = random.randint(1, 978)
    plt.title('pred: ' + str(y_pred[n]))
    plt.suptitle('true: ' + str(y_test[n]))
    plt.axis('off')
    fig.tight_layout()
    plt.imshow(X_test[n], cmap='gray')

'''