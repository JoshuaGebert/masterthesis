import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, roc_curve, plot_roc_curve, auc
from tensorflow import keras
from sklearn.ensemble import StackingClassifier
import pickle
import random
from datetime import datetime


datadir = r"F:\Evaluierungsdaten_Master\binary\Wuerfel\Wuerfel_clogged"
#datadir = r"F:\Evaluierungsdaten_Master\binary\Wuerfel_Grundkoerper\binary_gut"
#categories = ["no_gut", "gut"]
#categories = ["no_warping", "warping"]
#categories = ["no_layershift", "layershift"]
categories = ["no_clogged_nozzle", "clogged_nozzle"]
#categories = ["no_spaghetti", "spaghetti"]



IMG_height = 140
IMG_width = 310

training_data = []

def create_training_data():
    for category in categories:
        path = os.path.join(datadir, category)
        class_num =categories.index(category)
        print(path)
        for img in os.listdir(path):
            img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
            new_array = cv2.resize(img_array, (IMG_width, IMG_height))
            training_data.append([new_array,class_num])


create_training_data()

print(len(training_data))

random.shuffle(training_data)

X = []
y = []


for features, label in training_data:
    X.append(features)
    y.append(label)

X = np.array(X).reshape(-1, IMG_height, IMG_width, 1)
y = np.array(y)

X = X/255.0

model_4cnn_3d = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_6_4cnn-3dense\clogged")
model_4cnn = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_3_4cnn\clogged")
model_2cnn_3d = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_4_2cnn_3dense\clogged")



#y_pred= (model_warping.predict(X) > 0.5).astype("int32")
#y_pred = (model_layershift.predict(X) > 0.5).astype("int32")
#y_pred = (model_spaghetti.predict(X) > 0.5).astype("int32")
y_pred_4cnn_3d = model_4cnn_3d.predict(X)
y_pred_4cnn = model_4cnn.predict(X)
y_pred_2cnn_3d = model_2cnn_3d.predict(X)

#y_pred = (model_clogged.predict(X) > 0.5).astype("int32")
y_pred_1 = (model_2cnn_3d.predict(X) > 0.5).astype("int32")
y_pred_2 = (model_4cnn.predict(X) > 0.5).astype("int32")
y_pred_3 = (model_4cnn_3d.predict(X) > 0.5).astype("int32")
cm = confusion_matrix(y, y_pred_1)
print(cm)
cm = confusion_matrix(y, y_pred_2)
print(cm)
cm = confusion_matrix(y, y_pred_3)
print(cm)


#plot_roc_curve(model_gut, X, y)
#plt.show()

fpr1, tpr1, _ = roc_curve(y, y_pred_4cnn)
roc_auc1 = auc(fpr1, tpr1)
fpr2, tpr2, _ = roc_curve(y, y_pred_4cnn_3d)
roc_auc2 = auc(fpr2, tpr2)
fpr3, tpr3, _ = roc_curve(y, y_pred_2cnn_3d)
roc_auc3 = auc(fpr3, tpr3)

mycolor = "#008252"

plt.figure()
lw = 1
plt.plot(fpr3, tpr3, color='lightgrey',
lw=lw, label='ROC Kurve 2cnn_3dense (Fläche = %0.2f)' % roc_auc3)
plt.plot(fpr1, tpr1, color=mycolor,
lw=lw, label='ROC Kurve 4cnn_1dense (Fläche = %0.2f)' % roc_auc1)
plt.plot(fpr2, tpr2, color='grey',
lw=lw, label='ROC Kurve 4cnn_3dense (Fläche = %0.2f)' % roc_auc2)
plt.plot([0, 1], [0, 1], color='black', lw=1, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Clogged Nozzle')
plt.legend(loc="lower right")
plt.savefig("clogged.svg")
plt.show()








