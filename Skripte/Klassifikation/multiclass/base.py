import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import pickle
import random
from datetime import datetime

pickle_in = open("X_gesamt_ova.pickle","rb")
X = pickle.load(pickle_in)

pickle_in = open("y_gesamt_ova.pickle","rb")
y = pickle.load(pickle_in)

X = X/255.0


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)


model = Sequential()

model.add(Conv2D(64,(3,3), input_shape = X.shape[1:]))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.4))

model.add(Conv2D(64, (3,3)))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.4))

model.add(Flatten())
model.add(Dense(64))
model.add(Dropout(0.4))

model.add(Dense(5))
model.add(Activation("softmax"))

model.compile(loss="sparse_categorical_crossentropy",
              optimizer="adam",
              metrics=["accuracy"])

log_dir = "F:\Tensorboard\date_" + datetime.now().strftime("%Y%m%d-%H%M%S")\
          + "_ova_mehrklassen_wuerfel-base"
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, update_freq="batch", histogram_freq=1)


model.fit(X_train, y_train, batch_size=32, epochs=3, validation_split=0.1, callbacks=[tensorboard_callback])


model.save(r"F:\Modelle\binary\ova\mehrklassen_2")

model.evaluate(X_test, y_test)
y_pred = model.predict_classes(X_test)

cm = confusion_matrix(y_test, y_pred, )
print(cm)

fig = plt.figure(figsize=(20,20))

for i in range(10):
    fig.add_subplot(2, 5, i + 1)
    n = random.randint(1, 978)
    plt.title('pred: ' + str(y_pred[n]))
    plt.suptitle('true: ' + str(y_test[n]))
    plt.axis('off')
    fig.tight_layout()
    plt.imshow(X_test[n], cmap='gray')
    

plt.show()










