import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from tensorflow import keras
from sklearn.ensemble import StackingClassifier
import pickle
import random
from datetime import datetime


datadir = r"F:\Evaluierungsdaten_Master\Wuerfel_Grundkoerper"
#datadir = r"F:\Evaluierungsdaten_Master\newperspective_eval"
#categories = ["gut", "layershift"]
categories = ["gut", "warping","layershift","spaghetti","clogged_nozzle"]

IMG_height = 140
IMG_width = 310

for category in categories:
    path = os.path.join(datadir, category)
    print(path)
    for img in os.listdir(path):
        img_array = cv2.imread(os.path.join(path,img), cv2.IMREAD_GRAYSCALE)
        plt.imshow(img_array, cmap="gray")
        plt.show()
        break
    break

print(img_array.shape)

training_data = []

def create_training_data():
    for category in categories:
        path = os.path.join(datadir, category)
        class_num =categories.index(category)
        print(path)
        for img in os.listdir(path):
            img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
            new_array = cv2.resize(img_array, (IMG_width, IMG_height))
            training_data.append([new_array,class_num])


create_training_data()

print(len(training_data))

random.shuffle(training_data)

X = []
y = []


for features, label in training_data:
    X.append(features)
    y.append(label)

X = np.array(X).reshape(-1, IMG_height, IMG_width, 1)
y = np.array(y)

X = X/255.0



model_warping = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_29_final_hyperparameter\warping")
#model_warping = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_12_augmentation_dropout\warping")
#model_warping = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_9_mobilenet\warping")
print("warping")
model_layershift = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_29_final_hyperparameter\layershift")
#model_layershift = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_21_earlystopping\patience_3\layershift")
#model_layershift = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_9_mobilenet\layershift")
print("layershift")
model_spaghetti = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_29_final_hyperparameter\spaghetti")
#model_spaghetti = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_21_earlystopping\patience_5\spaghetti")
#model_spaghetti = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_9_mobilenet\spaghetti")
print("spaghetti")
model_clogged = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_29_final_hyperparameter\clogged")
#model_clogged = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_21_earlystopping\patience_3\clogged")
#model_clogged = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_9_mobilenet\clogged")
print("clogged")
model_gut = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_29_final_hyperparameter\gut")
#model_gut = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_22_bigmodul\gut")
#model_gut = tf.keras.models.load_model(r"F:\Modelle\binary\ova\test_9_mobilenet\gut")
print("gut")
model_mehrklassen = tf.keras.models.load_model(r"C:\Users\User\Desktop\Output\inception_like")


model_gut.summary()

model_warping.summary()

model_layershift.summary()

model_spaghetti.summary()

model_clogged.summary()

'''
y_pred_warping = (model_warping.predict(X) > 0.5).astype("int32")
y_pred_layershift = (model_layershift.predict(X) > 0.5).astype("int32")
y_pred_spaghetti = (model_spaghetti.predict(X) > 0.5).astype("int32")
y_pred_clogged = (model_clogged.predict(X) > 0.5).astype("int32")
y_pred_gut = (model_gut.predict(X) > 0.5).astype("int32")



y_pred_mehrklassen = np.argmax(model_mehrklassen.predict(X), axis=-1)
length = len(y)

cm = confusion_matrix(y, y_pred_mehrklassen)
print(cm)
model_mehrklassen.evaluate(X, y)

cm = confusion_matrix(y, y_pred)
print(cm)
model.evaluate(X, y)
fig = plt.figure(figsize=(20,20))

for i in range(10):
    fig.add_subplot(2,5,i+1)
    plt.title(y_pred[i])
    plt.imshow(X[i], cmap='gray')

plt.show()


y_pred_ova = []
for i in range(length):
    pred = []
    pred.append(y_pred_gut[i])
    pred.append(y_pred_warping[i])
    pred.append(y_pred_layershift[i])
    pred.append(y_pred_spaghetti[i])
    pred.append(y_pred_clogged[i])
    if pred[0] == 1:
        y_pred_ova.append(0)
    elif pred[1] == 1:
        y_pred_ova.append(1)
    elif pred[2] == 1:
        y_pred_ova.append(2)
    elif pred[3] == 1:
        y_pred_ova.append(3)
    elif pred[4] == 1:
        y_pred_ova.append(4)
    else: y_pred_ova.append(0)

cm = confusion_matrix(y, y_pred_ova)
print(cm)
'''
y_pred_mehrklassen = np.argmax(model_mehrklassen.predict(X), axis=-1)
cm = confusion_matrix(y, y_pred_mehrklassen)
print(cm)



length = len(y)
y_pred_warping_1 = model_warping.predict(X)
y_pred_layershift_1 = model_layershift.predict(X)
y_pred_spaghetti_1 = model_spaghetti.predict(X)
y_pred_clogged_1 = model_clogged.predict(X)
y_pred_gut_1 = model_gut.predict(X)

y_pred_ova_1 = []
for i in range(length):
    pred = []
    pred.append(y_pred_gut_1[i])
    pred.append(y_pred_warping_1[i])
    pred.append(y_pred_layershift_1[i])
    pred.append(y_pred_spaghetti_1[i])
    pred.append(y_pred_clogged_1[i])
    vorh = pred.index(max(pred))
    y_pred_ova_1.append(vorh)

cm = confusion_matrix(y, y_pred_ova_1)
print(cm)

length = len(y_pred_ova_1)
falsch = []
falsch_label = []
gut_label = []
for i in range(length):
    if y[i] != y_pred_ova_1[i]:
        falsch.append(i)
        falsch_label.append(y_pred_ova_1[i])
        gut_label.append(y[i])
print(len(falsch))

for i in range(len(falsch)):
    titel = "true: " + str(gut_label[i]) + "pred: " + str(falsch_label[i])
    plt.title(titel)
    plt.imshow(X[falsch[i]], cmap='gray')
    os.chdir(r"F:\Evaluierungsdaten_Master\FalscheKlassifikation\eval")
    plt.savefig('Fehler' + str(i) +'.png', transparent= True)
    plt.show()









