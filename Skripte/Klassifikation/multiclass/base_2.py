import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import pickle
import random
from datetime import datetime

datadir = r"F:\Trainingsdaten_Master\Wuerfel"
categories = ["gut", "warping","layershift","spaghetti","clogged_nozzle"]
IMG_height = 140
IMG_width = 310


training_data = []

def create_training_data():
    for category in categories:
        path = os.path.join(datadir, category)
        class_num =categories.index(category)
        print(path)
        for img in os.listdir(path):
            img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
            new_array = cv2.resize(img_array, (IMG_width, IMG_height))
            training_data.append([new_array,class_num])


create_training_data()

print(len(training_data))

random.shuffle(training_data)

X = []
y = []

for features, label in training_data:
    X.append(features)
    y.append(label)

X = np.array(X).reshape(-1, IMG_height, IMG_width, 1)
y = np.array(y)

X = X / 255.0

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

model = Sequential()

model.add(Conv2D(64, (3, 3), input_shape=X.shape[1:]))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3)))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dense(64))

model.add(Dense(5))
model.add(Activation("softmax"))

model.compile(loss="sparse_categorical_crossentropy",
              optimizer="adam",
              metrics=["accuracy"])

log_dir = "F:\Tensorboard\date_" + datetime.now().strftime("%Y%m%d-%H%M%S") \
          + "_mehrklassen_wuerfel-base"
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, update_freq="batch", histogram_freq=1)

model.fit(X_train, y_train, batch_size=32, epochs=3, validation_split=0.1, callbacks=[tensorboard_callback])

model.save(r"F:\Modelle\mehrklassen")

model.evaluate(X_test, y_test)
y_pred = model.predict_classes(X_test)

cm = confusion_matrix(y_test, y_pred, )
print(cm)

fig = plt.figure(figsize=(20, 20))

for i in range(10):
    fig.add_subplot(2, 5, i + 1)
    n = random.randint(1, 978)
    plt.title('pred: ' + str(y_pred[n]))
    plt.suptitle('true: ' + str(y_test[n]))
    plt.axis('off')
    fig.tight_layout()
    plt.imshow(X_test[n], cmap='gray')

plt.show()
