import os
import cv2
import matplotlib as plt

from pathlib import Path
from datetime import datetime
from collections import namedtuple

import numpy as np
File = namedtuple('File', 'name_path')

files = []
p = Path("E:/Trainingsdaten_Master/Trainingsdaten/Trainingsdatensaetze/Datensatz_Klassifikation/train/")
i = 0

# Ordner mit Inhalt Struktur der Daten
data_dir = r"E:/Trainingsdaten_Master/Trainingsdaten/Trainingsdatensaetze/Datensatz_Klassifikation/train/"
data_dir = Path(data_dir)  # liest den Pfad ein

# Labels in Array schreiben
label_dir = data_dir / 'labels'
label_names = np.array([item.name for item in label_dir.glob('*')])

for x in label_names:
    p = Path("E:/Trainingsdaten_Master/Trainingsdaten/Trainingsdatensaetze/Datensatz_Klassifikation/train/labels/" + x)
    print(p)
    for item in p.glob('**/*/*'):
        i = i + 1
        print(item)
        if item.suffix in ['.png','.xlsx']:
            print(item)

            img = cv2.imread(str(item))
            imgL = cv2.Laplacian(img, cv2.CV_64F)
            canny = cv2.Canny(img, 100, 200)
            os.chdir("E:/Trainingsdaten_Master/Trainingsdaten/Trainingsdatensaetze/Datensatz_Sobel/Laplacian/" + x)
            cv2.imwrite("Sobel_Laplacian_ " + str(i) + ".png",imgL)
            os.chdir("E:/Trainingsdaten_Master/Trainingsdaten/Trainingsdatensaetze/Datensatz_Sobel/Canny/" + x )
            cv2.imwrite("Sobel_Canny_ " + str(i) +  ".png", canny)


