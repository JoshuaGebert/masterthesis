import tensorflow as tf
import tensorflow_addons as tfa
import io
import matplotlib as plt
import matplotlib.pyplot as plt
import numpy as np
import scipy.ndimage as nd
import cv2

file_path = r"E:\Trainingsdaten_Master\Trainingsdaten\Trainingsdatensaetze\Datensatz_Klassifikation\train\cad\Wuerfel.png"
#file_path = "E:\Trainingsdaten_Master\Trainingsdaten\Trainingsdatensaetze\Datensatz_Klassifikation\train\labels\gut\Wuerfel_layershift_links_15.png"
print("success")



def load_img(path_to_img):
  max_dim = 512
  img = tf.io.read_file(path_to_img)
  img = tf.image.decode_image(img, channels=3)
  img = tf.image.convert_image_dtype(img, tf.float32)

  shape = tf.cast(tf.shape(img)[:-1], tf.float32)
  long_dim = max(shape)
  scale = max_dim / long_dim

  new_shape = tf.cast(shape * scale, tf.int32)

  img = tf.image.resize(img, new_shape)
  img = img[tf.newaxis, :]
  return img


img = load_img(file_path)


plt.figure(figsize=(14,10))

sobel = tf.image.sobel_edges(img)

dx = nd.sobel(sobel,1)
dy = nd.sobel(sobel,0)
mag = np.hypot(dx,dy)
mag *= 255.0/nd.max(mag)

fig, ax = plt.subplots()
ax.imshow(mag, cmap = 'gray')
plt.xticks([]), plt.yticks([])
plt.show()

