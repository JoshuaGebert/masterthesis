import tensorflow as tf
from keras import layers, Sequential
import numpy as np
import keras
import pathlib
from pathlib import Path
from datetime import datetime
from tensorflow.keras.callbacks import TensorBoard, EarlyStopping

batch_size = 16
img_height = 140
img_width = 310

data_dir = r"E:\Trainingsdaten_Master\Trainingsdaten\Trainingsdatensaetze\Datensatz_Klassifikation\train\labels"
train_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="training",
  seed=123,
  image_size=(img_height, img_width),
  batch_size=batch_size)

directory = r'E:\Trainingsdaten_Master\Trainingsdaten\Trainingsdatensaetze\Datensatz_Klassifikation\predict'
data_dir = pathlib.Path(data_dir)  # liest den Pfad ein
list_ds = tf.data.Dataset.list_files([str(data_dir / '*')])
print(len(list_ds))
print(list_ds)
class_names = train_ds.class_names
print(class_names)
model = tf.keras.models.load_model(r'C:\Users\User\masterthesis\Code\Skripte_fzi\Training_output_Klassifikation\Modell_speicher\nocad')

